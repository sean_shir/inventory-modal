var dest = './build',
  src = './src',
  mui = './node_modules/material-ui/src';

module.exports = {
  browserSync: {
    proxy: {
      target: 'localhost:3000',
      middleware: function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*')
        next()
      }
    },
    server: {
      // We're serving the src folder as well
      // for sass sourcemap linking
      baseDir: [dest, src]
    },
    files: [
      dest + '/**'
    ],
    port: 3000
  },
  markup: {
    src: src + "/www/**",
    dest: dest
  },
  browserify: {
    // Enable source maps
    debug: false,//true,
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: src + '/app/app.jsx',
      dest: dest + '/js/',
      outputName: 'app.js'
    }
    ]
  }
};
