## Admin App
Admin App in React

## Prerequisites

```sh
node
```

```sh
gulp
```
If you dont have gulp, install it by 
```sh
npm install gulp -g
```

## Installation
install dependencies:

cd <project folder>adminapp
```sh
npm install
```
For development mode, run json server:
```
json-server --watch db.json --port 3004
```
Now you can run your local server:
```sh
npm start
```
or 
```sh
gulp
```
Server is located at http://localhost:3000