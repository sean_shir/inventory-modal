var React = require('react');
let  ReactCSSTransitionGroup = require('react-addons-css-transition-group');
let StaticContainer = require('react-static-container');
var { createHistory, useBasename } =require('history');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var { Route, RouteHandler, IndexRoute, Link, History } = ReactRouter;
var Router = ReactRouter.Router;
var Navigation = ReactRouter.Navigation;	
var LoginStore = require('../stores/LoginStore.js') 

 

class RouteCSSTransitionGroup extends React.Component {
	static contextTypes = {
		location: React.PropTypes.object
	}

	constructor(props, context) {
		super(props, context)

		this.state = {
			previousPathname: null
		}
	}
	mixins: [History]

	componentWillReceiveProps(nextProps, nextContext) {
		if (!LoginStore.isLoggedIn()) {
       this.history.pushState(null, "/State/");
      }
		if (nextContext.location.pathname !== this.context.location.pathname) {
			this.setState({ previousPathname: this.context.location.pathname })
		}
	}

	render() {
		const { children, ...props } = this.props
		const { previousPathname } = this.state

		return (
			/*<ReactCSSTransitionGroup {...props}>*/
			<StaticContainer
			key={previousPathname || this.context.location.pathname}
			shouldUpdate={!previousPathname}
			>
			{children}
			</StaticContainer>
		/*	</ReactCSSTransitionGroup>*/
			)
	}

	componentDidUpdate() {
		if (this.state.previousPathname) {
			this.setState({ previousPathname: null })
		}
	}
}

var Main= React.createClass({
	render(){
		const { pathname } = this.props.location

		return(
			<div className="main container-fluid ">
			<header className="main-header row">
			<a href="#/" className="logo">
		      <span className="logo-lg"><b>adminapp</b></span>
		    </a>
		    <nav className="navbar navbar-static-top" role="navigation">
		      <div className="navbar-custom-menu">
		        <ul className="nav navbar-nav">
		          <li className="dropdown user user-menu">
		            <a href="" className="dropdown-toggle" data-toggle="dropdown">
		              <img src="images/dummyimg.png" className="user-image" alt="User Image" />
		              <span className="hidden-xs">Account Settings</span>
		            </a>
		            <ul className="dropdown-menu">
		              <li className="user-header">
		                <img src="images/dummyimg.png" className="img-circle" alt="User Image" />
		                <p>
		                  Account Settings
		                </p>
		              </li>
		              
		              <li className="user-footer">
		                <div className="pull-left">
		                  <a href="" className="btn btn-default btn-flat">Profile</a>
		                </div>
		                <div className="pull-right">
		                  <a href="" className="btn btn-default btn-flat">Sign out</a>
		                </div>
		              </li>
		            </ul>
		          </li>
		         </ul>
		      </div>

		    </nav>
				
			</header>
			<section className="main_section row">
			<RouteCSSTransitionGroup component="div" transitionName="example" className="layout-vertical"
				transitionEnterTimeout={500} transitionLeaveTimeout={500}
				>
			{React.cloneElement(this.props.children || <div/>, { key: pathname })}
			</RouteCSSTransitionGroup>

				
			</section>
			</div>
			)
	}
});

var App = React.createClass({

	contextTypes: {
		router: React.PropTypes.func
	},

	
	render() {

		return (
			<div style={{height:'100%'}}>
			<aside className="main-sidebar">
				<section className="sidebar">
					<ul className="sidebar-menu">
						<li><Link to="/Inventory" activeClassName="active">Inventory <i className="fa fa-angle-right pull-right"></i></Link></li>
						<li><Link to="/State" activeClassName="active">State Type <i className="fa fa-angle-right pull-right"></i></Link></li>
						<li><Link to="/Sensor" activeClassName="active">Sensor Type <i className="fa fa-angle-right pull-right"></i></Link></li>
						<li><Link to="/Thing" activeClassName="active">Thing Type <i className="fa fa-angle-right pull-right"></i></Link></li>						
						<li><Link to="/Rule" activeClassName="active">Rules <i className="fa fa-angle-right pull-right"></i></Link></li>

					</ul>
				</section>
			</aside>
			<div className="content-wrapper">
				<section className="right-section">
				
				{this.props.children}
				</section>
			</div>
			</div>
			);
	}
});

let Login = require('./Login.js');
let Register = require('./Register.js');

{/*Invetory list*/}
let DevicesModule = require('./Inventory/DevicesModule.js');

{/*State list*/}
let State = require('./State/State.jsx');
let StateList = require('./State/StateList.jsx');
let CreateState = require('./State/CreateState.jsx');

{/*Rules*/}
let Rule = require('./Rules/Rules.js');
let RuleList = require('./Rules/RuleList.js');
let CreateRule = require('./Rules/CreateRule.js'); 

{/*Conditions*/}
let Conditions = require('./Conditions/Conditions.js');
let CreateSingleCondition = require('./Conditions/CreateSingleCondition.js');
let CreateMultiCondition = require('./Conditions/CreateMultiCondition.js');

{/*Sensor*/}
let Sensor = require('./Sensor/Sensor.js');
let SensorList = require('./Sensor/SensorList.js');
let CreateSensor = require('./Sensor/CreateSensor.js');

{/*Thing*/}
let Thing = require('./Thing/Thing.js');
let ThingList = require('./Thing/ThingList.js');
let CreateThing = require('./Thing/CreateThing.jsx');


var routes = (
	<Router history={history}>
		<Route path="/" component={Main} name="Main">
			<IndexRoute component={Login} name="Login"/> 
			<Route path="/login" component={Login} name="Login"/> 
			<Route path="/register" component={Register} name="Register"/> 
			<Route path="/App" component={App} name="App">
				<IndexRoute component={State} name="State"/>

				<Route name="Inventory" path="/Inventory" component={DevicesModule}>
				</Route>

				<Route name="StateList" path="/State" component={State}>
					<IndexRoute component={StateList} name="StateList"/>
					<Route name="CreateState" path="/CreateState" component={CreateState} />
					<Route name="EditState" path="/EditState/:StateId" component={CreateState} />
				</Route>

				<Route name="Rule" path="/Rule" component={Rule}> 
					<IndexRoute component={RuleList} name="RuleList"/>
					<Route name="CreateRule" path="/CreateRule" component={CreateRule} />
					<Route name="EditRule" path="/EditRule/:RuleId" component={CreateRule} /> 
					<Route name="CreateSingleCondition" path="/CreateSingleCondition" component={CreateSingleCondition} />
					<Route name="CreateMultiCondition" path="/CreateMultiCondition" component={CreateMultiCondition} />
				</Route>

				<Route name="Sensor" path="/Sensor" component={Sensor}> 
					<IndexRoute component={SensorList} name="SensorList"/>
					<Route name="CreateSensor" path="/CreateSensor" component={CreateSensor} />
					<Route name="EditSensor" path="/EditSensor/:SensorId" component={CreateSensor} />
				</Route>

				<Route name="Thing" path="/Thing" component={Thing}> 
					<IndexRoute component={ThingList} name="ThingList"/>
					<Route name="CreateThing" path="/CreateThing" component={CreateThing} />
					<Route name="EditThing" path="/EditThing/:ThingId" component={CreateThing} />
				</Route>
			</Route>
				
		</Route>
	</Router>
	);

ReactDOM.render(<Router>{routes}</Router>,  document.getElementById('app'))