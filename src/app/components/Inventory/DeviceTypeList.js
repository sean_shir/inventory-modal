'use strict'

var React = require('react'),
    Mixins = require('../../mixins/Mixins'),
    Actions = require('../../actions/Actions'),
    Devices = require('../../services/Devices')
import { Button, Glyphicon, FormControls } from 'react-bootstrap'

function getDevices() {
  return {
    devices: Devices.get_devices().map( (d,i) => { return d } )
  }
}

module.exports = React.createClass({
  mixins: [Mixins(getDevices)],
  listItemHover: function(i,e) {
    let devices = this.state.devices.map( (v,k) => {
      let things = v.things == undefined ? [] : v.things
      v["class2"] = k == i ? 'hover' : ""
      things.map((t,i)=>{
        v.things[i].icon = t.thingCatagory == 'sensor' ? '/images/signal.png' : '/images/bulboff.png'
      })
      return v
    })
    this.setState({devices:devices})
    Actions.setElementDevice(devices[i])
  },
  render: function() {
    return (
      <div className="devices-list">
        <ul className="menu-list">
          {this.state.devices.map((e,i)=>{
            let glyph = e.class2 == "hover" ? (<i className="fa fa-caret-right"></i>) : ""
            return (
              <li key={i} className={"cursor "+e.class2} onMouseOver={this.listItemHover.bind(null,i,e)}>
                {e.deviceType}
                {glyph}
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
})
