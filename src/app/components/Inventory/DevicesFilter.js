'use strict'

var React = require('react')
import { MenuItem, NavDropdown, Glyphicon } from 'react-bootstrap'

module.exports = React.createClass({
  getInitialState: function() {
    return {
      filter: 'Device Type',
      classD: (<i className="fa fa-check"></i>),
      classO: ''
    }
  },
  filterSelect: function(e) {
    let f = e ? 'OEM' : 'Device Type',
        c = (<i className="fa fa-check"></i>)
    e ? this.setState({classD:'',classO:c}) : this.setState({classD:c,classO:''})
    this.setState({filter:f})
    this.props.filterResult(e)
  },
  render: function() {
    return (
      <div className="filter filter-device-type">
        <NavDropdown className="" eventKey={2} title={'Filter by: '+this.state.filter} id="filter-devices">
          <div eventKey="2.1" href="#" onClick={this.filterSelect.bind(null,0)}>Device Type {this.state.classD}</div>
          <div eventKey="2.2" href="#" onClick={this.filterSelect.bind(null,1)}>OEM {this.state.classO}</div>
        </NavDropdown>
      </div>
    )
  }
})
