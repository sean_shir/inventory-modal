 'use strict'

var React = require('react'),
    Actions = require('../../actions/Actions'),
    Devices = require('../../services/Devices')
import { Button, Glyphicon, FormControls } from 'react-bootstrap'

function getDevices() {
  let list = []
  Devices.get_devices().map((d,i)=>{
    if (!list.length) list = [{name:d.name,elements:[d]}]
    else {
      list.map((l,j)=>{
        let flag = 1
        if(d.name==l.name){
          list[j].elements.push(d)
          flag = 0
        }
        if (list.length==j+1 && flag) list.push({name:d.name,elements:[d]})
      })
    }
  })
  return {
    devices: list
  }
}

module.exports = React.createClass({
  getInitialState: function() {
    return getDevices()
  },
  listItemHover: function(i,e) {
    let devices = this.state.devices.map( (v,k) => {
      v["class2"] = k == i ? 'hover' : ""
      v.elements.map((e,j)=>{
        let things = e.things == undefined ? [] : e.things
        things.map((t,i)=>{
          v.elements[j].things[i].icon = t.thingCatagory == 'sensor' ? '/images/signal.png' : '/images/bulboff.png'
        })
      })
      return v
    })
    this.setState({devices:devices})
    Actions.setOEM(devices[i])
    Actions.setOEMElements(devices[i].elements)
    Actions.setDisplay(0)
  },
  render: function() {
    return (
      <div className="devices-list">
        <ul className="menu-list">
          {this.state.devices.map((e,i)=>{
            let glyph = e.class2 == "hover" ? (<i className="fa fa-caret-right"></i>) : ""
            return (
              <li key={i} className={e.class2} onMouseOver={this.listItemHover.bind(null,i,e)}>
                {e.name}
                {glyph}
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
})
