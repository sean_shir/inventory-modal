var React = require('react');
var History = require('react-router').History;

var Modal = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],
  show:function(){
  	var id = this.props.id;
  	$('#'+id).modal('show');
  },
  hide:function(){
  	var id = this.props.id;
  	$('#'+id).modal('hide');
  },




  render: function () {
	return (
		<div className="modal fade" id={this.props.id} role="dialog">
		  <div className="modal-dialog my-modal" role="document">
		    <div className="modal-content">
		      <div className="modal-header">
		        <button type="button" className="close" onClick={this.hide}><span aria-hidden="true">&times;</span></button>
		        <h4 className="modal-title" id="myModalLabel">{this.props.title}</h4>
		      </div>
		      <div className="modal-body">
		        {this.props.children}
		      </div>
		      <div className="modal-footer text-right">
		        {this.props.actions}
		      </div>
		    </div>
		  </div>
		</div>
	);
  },


});

module.exports = Modal;
