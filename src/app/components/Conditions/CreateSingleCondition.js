var React = require('react');
var History = require('react-router').History;
var SingleEventForm = require('./SingleEventForm.js');
//var MultipleEventForm = require('./MultipleEventForm.js');
var DataGenerator = require('../../services/DataGenerator.js');


var CreateSingleCondition= React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],

  getInitialState: function() {
      return {
      	ConditionData: this.props.data || DataGenerator.GenerateConditionData('single'),
        pagetitle:'Create Condition',
        eventType:'single',
        callback: this.props.Callback
      };
    },
  /*ChangeEventType:function(e){
  	var ConditionData =  DataGenerator.GenerateConditionData(e)
  	this.setState({eventType:e,ConditionData:ConditionData})
  },*/
  componentWillReceiveProps(nextProps) {
    if (nextProps.data) {
      this.setState({ConditionData: nextProps.data});
    }
  },

  render: function () {

 var actionform = <div><SingleEventForm Callback={this.state.callback} Condition={this.state.ConditionData}/></div>;
    /*if(this.state.eventType == 'single')
    	actionform = <div><SingleEventForm Callback={this.state.callback} Condition={this.state.ConditionData}/></div>
    else if(this.state.eventType == 'multi')
    	actionform = <div><MultipleEventForm /></div>*/



return (
	<div className="create-state create-condition">
		<div className="page-content">
		<form className="single_event_form">
			<div className="clearfix"></div>
				{actionform}
			</form>
		</div>
	</div>
  );
},


});

module.exports = CreateSingleCondition;
