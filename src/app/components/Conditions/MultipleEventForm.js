var React = require('react');
var History = require('react-router').History;
var AggregationForm = require('./AggregationForm.js');
var GroupForm = require('./GroupForm.js');
var SequenceForm = require('./SequenceForm.js')
  var DataGenerator = require('../../services/DataGenerator.js');
var SmartSelect = require('../SmartSelect.js');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');

var MultipleEventForm= React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],

  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    this.formComponentInstance = null;
    return {
      eventType_m:'',
      loopvalue:2,
      ConditionData: null,
      timeFormats: {},
      timeUnit: '',
      timeWindow: 0,
      showValidationErrors: false,
    };
  },
  componentDidMount: function() {
    console.log('MultipleEventForm.componentDidMount, ConditionData', this.props.ConditionData);
    this.conditionPopulator.getTimeFormats(result => {
      this.setState({timeFormats: result});
    });
  },
  componentWillReceiveProps: function(nextProps) {
    console.log('MultipleEventForm.componentWillReceiveProps, ConditionData', nextProps.ConditionData);
    if (nextProps.ConditionData) {
      let type = nextProps.ConditionData.eventFlow.type;
      type = type === 'groupsequence' ? 'group' : type;
      this.setState({
        ConditionData: nextProps.ConditionData,
        eventType_m: type,
        loopvalue: nextProps.ConditionData.eventFlow.numberOfEvents,
        showValidationErrors: false,
      });

    }
  },
  componentDidUpdate() {
    console.log('MultipleEventForm, componentDidUpdate');
  },
  ChangeEventM: function(){
    var eventNameM = this.refs.ChangeEventdd.value;
    console.log('MultipleEventForm ChangeEventM, eventNameM: ', eventNameM);

    var ConditionData = DataGenerator.GenerateMultiSequenceConditionData(eventNameM);
    this.saveTimeWindowToCondition(ConditionData, this.state.timeWindow, this.state.timeUnit);
    this.setState({
      eventType_m:eventNameM,
      ConditionData:ConditionData,
      showValidationErrors: false,
    });
    console.log(ConditionData)

  },
  updateLoop:function(){
    var loopvalue = Number(this.refs.updateloop.value);
    this.setState({loopvalue:loopvalue})
  },
  SaveCondition:function(e){
    e.preventDefault();
    if (!this.formComponentInstance ||
        !this.formComponentInstance.isFormValid())
    {
      this.setState({showValidationErrors: true});
      return;
    }

    localStorage.setItem('tempCondition', JSON.stringify(this.state.ConditionData));
    this.props.Callback();
    //this.history.goBack();
  },

  timeUnitChanged: function(v) {
    this.setState({timeUnit: v});
    this.saveTimeWindowToCondition(this.state.ConditionData, this.state.timeWindow, v);
    // TODO set event cond time window
  },
  timeWindowChanged: function(e) {
    this.setState({timeWindow: e.target.value});
    this.saveTimeWindowToCondition(this.state.ConditionData, e.target.value, this.state.timeUnit);
  },
  saveTimeWindowToCondition: function(data, timeWindow, timeUnit) {
    console.log('£££ ');
    if (data) {
      console.log('£££% ', timeWindow + ' ' + timeUnit);
      data.eventFlow.timeWindow = timeWindow + ' ' + timeUnit;
    }
  },

  render: function () {
    let updateFormRef = (inst) => {
      this.formComponentInstance = inst;
    };
    var multiformType =  ''
    if(this.state.eventType_m =='')
      multiformType = '';
    else if(this.state.eventType_m == 'sequence') {
      multiformType =
        <div key={this.state.eventType_m}>
          <SequenceForm data={this.state.ConditionData}
            showValidationErrors={this.state.showValidationErrors}
            ref={updateFormRef}/>
        </div>
    } else if(this.state.eventType_m == 'group' ||
              this.state.eventType_m == 'groupsequence') {
      multiformType =
        <div key={this.state.eventType_m}>
          <GroupForm loopvalue={this.state.loopvalue}
            showValidationErrors={this.state.showValidationErrors}
            ref={updateFormRef}
            data={this.state.ConditionData}/>
        </div>
    } else if(this.state.eventType_m == 'aggregation') {
      multiformType =
        <div key={this.state.eventType_m}>
          <AggregationForm data={this.state.ConditionData}
            showValidationErrors={this.state.showValidationErrors}
            ref={updateFormRef}/>
        </div>
    }



    let showValidationErrors = this.state.showValidationErrors &&
                               (!this.formComponentInstance ||
                                !this.formComponentInstance.isFormValid());

      return (
      <fieldset>
        <div className=" form-group form-inline MultipleEventForm">
          <div className="col-sm-12">
            <select ref="ChangeEventdd" className="wauto"
              value={this.state.eventType_m}
              style={{width:'20%'}} onChange={this.ChangeEventM}>
              <option value="">Select</option>
              <option value="sequence">Sequence</option>
              <option value="group">Group</option>
              <option value="aggregation">Aggregation</option>
            </select>
            <span> for every </span>
            <select value={this.state.loopvalue} className="wauto"
              style={{width:'5%'}} onChange={this.updateLoop}
              ref="updateloop">
              {
                _.range(1,31).map(i => {
                  return <option value={i}>{i}</option>
                })
              }
            </select>
            <span> within </span>
            <input type="number" className="wauto"
              onChange={this.timeWindowChanged}
              style={{width:'50px', marginRight:'15px'}} />

            <SmartSelect
              className="wauto" style={{width:'150px'}}
              value={this.state.timeUnit}
              onSetValue={this.timeUnitChanged}
              items={
                _.map(this.state.timeFormats, (v,k) => {
                  return {label: k, value: v};
                })
              } />
          </div>
          <div className="clearfix"></div>
          {multiformType}
        </div>
        <div className="form-actions">
          {
            !showValidationErrors ? null :
              <div className="form-error-message">
                Please fill out all the forms
              </div>
          }
          <button type="submit" className="btn btn-red pull-right" onClick={this.SaveCondition}>Add</button>
        </div>
      </fieldset>




      );
  },


});

module.exports = MultipleEventForm;
