var React = require('react');
var History = require('react-router').History;
var SmartSelect = require('../SmartSelect.js');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');
var {getPathsAndPropertiesOf} = require('./common.js');

var GroupForm= React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],

  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    let conds = this.props.data.eventFlow.events.map(() =>
      this.createEmptyCond()
    );
    return {
      pattern: [],
      ConditionData:this.props.data,
      listClass: this.props.data.eventFlow.type === 'groupsequence'
        ? 'AggregatinListNumber Aggregation_list' : 'Aggregation_list',
      operators: [],
      conds,
    };
  },
  /*getDefaultProps: function() {
     return {
     patterns: 1
     };
     },*/
  createEmptyCond:function() {
    return {
      labelOrThing: '',
      fields: {
        paths: [],
        properties: {},
      }
    };
  },

  numberOfEventsChanged:function() {
    let eventFlow = this.state.ConditionData.eventFlow;
    let numberOfEvents = Number(eventFlow.numberOfEvents);

    if (eventFlow.events.length > numberOfEvents) {
      eventFlow.events.splice(numberOfEvents);
    }
    while(eventFlow.events.length < numberOfEvents) {
      eventFlow.events.push(
        {
          conditions: [
            {
              "thingName": "","dataType": "","property": "","operator": "","operand": ""
            }
          ]
        }
      );
    }

    if (this.state.conds.length > numberOfEvents) {
      this.state.conds.splice(numberOfEvents);
    }
    while(this.state.conds.length < numberOfEvents) {
      this.state.conds.push(this.createEmptyCond());
    }
    this.forceUpdate();
  },

  componentDidMount:function(){
    var ConditionData = this.state.ConditionData;
    let {loopvalue} = this.props;
    ConditionData.eventFlow.numberOfEvents = loopvalue;
    this.numberOfEventsChanged();
    //blankl
    this.setState({ConditionData})
    console.log(this.state.ConditionData)
    console.log('GroupForm.componentDidMount, data: ', this.props.data);

    this.conditionPopulator.getOperators((result) => {
      this.setState({operators: result});

      if (this.props.data) {
        this.initializeFields(this.props.data);
      }
    });
  },
  componentDidUpdate:function() {
    console.log('GroupForm.componentDidUpdate', this.state);
    var {eventFlow} = this.state.ConditionData;
    let {loopvalue} = this.props;
    if (Number(eventFlow.numberOfEvents) !== Number(loopvalue)) {
      eventFlow.numberOfEvents = loopvalue;
      this.numberOfEventsChanged();
    }
  },

  componentWillReceiveProps:function(nextProps){
    console.log('GroupForm.componentWillReceiveProps, data: ', nextProps.data);
    if(this.props.data != nextProps.data){
      console.log('GroupForm.componentWillReceiveProps, nextProps.data is different');
      this.initializeFields(_.cloneDeep(nextProps.data));
      this.setState({ConditionData:nextProps.data, showValidationErrors: false});
    }
  },

  initializeFields:function(initConditionData) {
    console.log('GroupForm, initializeFields started');
    let events = initConditionData.eventFlow.events;
    if (!events[0].conditions[0].thingName && !events[0].conditions[0].labelName) {
      console.log('GroupForm, initializeFields skipped');
      return;
    }

    let promises = events.map(e => {
      return getPathsAndPropertiesOf(e.conditions[0].labelName,
                                     e.conditions[0].thingName);
    });
    Promise.all(promises).then(results => {
      console.log('GroupForm promises resolved: ', results);
      //let newConds = _.range(events.count).map(x => this.createEmptyCond());
      _.extend(this.state.ConditionData, initConditionData);
      console.log('GroupForm new ConditionData: ', this.state.ConditionData);
      let newConds = results.map((r,i) => {
        return {
          labelOrThing: events[i].conditions[0].labelName ? 'Label' : 'Thing',
          fields: {
            paths: r.paths,
            properties: r.properties,
          },
        };
      });
      this.setState({conds: newConds});
    });
  },

  ChangeEventsSeq:function(){
    let isGroupSeq = this.refs.eventbyref.checked;

    if (isGroupSeq) {
      this.populateAllWithFirstCondition();
    }

    var listClass = isGroupSeq ? 'AggregatinListNumber Aggregation_list':'Aggregation_list'

    this.state.ConditionData.eventFlow.type = isGroupSeq ? 'groupsequence' : 'group';
    this.setState({listClass:listClass});
  },
  updateSequence:function(index,field,v){
    console.log(v,index,field);
    var condition= this.state.ConditionData;
    condition.eventFlow.events[index].conditions[0][field] = v;
    this.setState({ConditionData:condition})
      console.log(this.state.ConditionData)
  },
  isFormValid: function() {
    let events = this.state.ConditionData.eventFlow.events;
    return _.every(events, e => {
      let d = e.conditions[0];
      return (d.labelName || d.thingName) &&
             d.property && d.operator && d.operand;
    });

  },

  populateByLabelOrThing:function(cond, condData, labelOrThing) {
    let update = (paths) => {
      console.log('updating paths', paths);
      delete condData['labelName'];
      delete condData['thingName'];
      cond.fields = {paths, properties: {}};
      //this.populateByPath(cond, condData, labelOrThing, ');
      this.forceUpdate();
    };

    if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabels(update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThings(update);
    } else {
      update([]);
    }

  },

  populateByPath:function(cond, condData, labelOrThing, path) {
    let update = (properties) => {
      condData.property = '';
      cond.fields.properties = properties;
      this.forceUpdate();
    };

    if (!path) {
      update([]);
    } else if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabelProperties(path, update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThingProperties(path, update);
    } else {
      update([]);
    }
  },

  populateAllWithFirstCondition(labelOrThing, paths) {
    let allCondData = this.state.ConditionData.eventFlow.events;
    let allConds = this.state.conds;
    let firstCond = allConds[0];
    let firstCondData = allCondData[0].conditions[0];

    for (let i=1; i<allCondData.length; i++) {
      let cond = allConds[i];
      let condData = allCondData[i].conditions[0];

      if (firstCondData.labelName) condData.labelName = firstCondData.labelName;
      if (firstCondData.thingName) condData.thingName = firstCondData.thingName;
      cond.labelOrThing = firstCond.labelOrThing;
      cond.fields = _.extend({}, firstCond.fields);
    }
  },

  condLabelOrThingChanged:function(i, v) {
    let cond = this.state.conds[i];
    let allCondData = this.state.ConditionData.eventFlow.events;
    let condData = allCondData[i].conditions[0];
    cond.labelOrThing = v;

    this.populateByLabelOrThing(cond, condData, v);
    this.forceUpdate();
  },

  condPathChanged:function(i, v) {
    let cond = this.state.conds[i];
    let condData = this.state.ConditionData.eventFlow.events[i].conditions[0];

    delete condData['labelName'];
    delete condData['thingName'];
    if (cond.labelOrThing === 'Label') {
      condData.labelName = v;
    } else if (cond.labelOrThing === 'Thing') {
      condData.thingName = v;
    }
    this.populateByPath(cond, condData, cond.labelOrThing, v);
    this.forceUpdate();
  },

  render: function () {

    console.log('GroupForm render, ConditionData: ', this.state.ConditionData);
    var listClass = this.state.listClass;
    var AggregationHTML  = this.state.ConditionData.eventFlow.events.map(function(event,i){
      let item = event.conditions[0];
      let cond = this.state.conds[i];
      let isLabel = cond.labelOrThing === 'Label';
      let isThing = cond.labelOrThing === 'Thing';
      let pathSelected = item.labelName && isLabel || item.thingName && isThing;
      let pSensor = cond.fields.properties.sensor || [];
      let pState = cond.fields.properties.state || [];
      let properties = [];
      if (pathSelected) {
        properties.push({
          label: 'Readings',
          items: pSensor.map(p => {
            return {label: p, value: p};
          })
        });
        if (isThing) {
          properties.push({
            label: 'Devices',
            items: pState.map(p => {
              return {label: p, value: p};
            })
          });
        }
      }

      return(

        <div className={listClass} key={i}>
          <div className="input-inline-gp">
            <div className="col-sm-3">
              <SmartSelect
                value={cond.labelOrThing}
                onSetValue={this.condLabelOrThingChanged.bind(this,i)}
                firstEmpty={true}
                items={[
                    {label: 'Thing ID', value: 'Thing'},
                    {label: 'Label ID', value: 'Label'},
                  ]} />
            </div>
            <div className="col-sm-4">
              <SmartSelect
                value={isLabel ? item.labelName : item.thingName}
                onSetValue={this.condPathChanged.bind(this,i)}
                firstEmpty={true}
                disabled={!cond.labelOrThing}
                items={
                  cond.fields.paths.map(p => {
                    return {label: p, value: p};
                  })
                } />
            </div>
            <div className="col-sm-4">
              <SmartSelect
                value={item.property}
                onSetValue={this.updateSequence.bind(this, i, 'property')}
                groupedItems={properties} />
            </div>

            <div className="col-sm-1">
              <SmartSelect
                value={item.operator}
                onSetValue={this.updateSequence.bind(this, i, 'operator')}
                disabled={!pathSelected}
                items={
                  this.state.operators.map(o => {
                    return {label: o, value: o}
                  })
                } />
            </div>
            <div className="col-sm-2">
              <input value={item.operand} type="text"  onChange={(e) => this.updateSequence(i, 'operand', e.target.value)}/>
            </div>
          </div>
        </div>


      )
    },this);
    var GropuFromHTML = <fieldset className="col-sm-12">
          <legend>Group Pattern:</legend>
          <div className="legend-right">
            <div className="checkbox">
              <input id="changeEvent"
                checked={this.state.ConditionData.eventFlow.type === 'groupsequence'}
                type="checkbox"
                onChange={this.ChangeEventsSeq}
                ref="eventbyref"/>
              <label htmlFor="changeEvent">Events by Sequence</label>
            </div>
          </div>
          <div className="form-group" style={{width:'100%'}}>
            {AggregationHTML}
          </div>

    </fieldset>

    return (
    <div className="form-inline">
      {GropuFromHTML}

    </div>


    );
  },


});

module.exports = GroupForm;
