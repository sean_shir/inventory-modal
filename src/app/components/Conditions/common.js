var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');

function getPathsAndPropertiesOf(labelName, thingName) {
  let promise = new Promise((resolve, reject) => {
    let conditionPopulator = new ConditionPopulator();

    let paths = [];

    let updateProperties = properties => {
      resolve({paths, properties});
    };

    let updatePaths = newPaths => {
      paths = newPaths;
      if (labelName) {
        conditionPopulator.getLabelProperties(labelName, updateProperties);
      } else {
        conditionPopulator.getThingProperties(thingName, updateProperties);
      }
    };


    if (labelName) {
      conditionPopulator.getLabels(updatePaths);
    } else {
      conditionPopulator.getThings(updatePaths);
    }
  });
  return promise;
}

module.exports = {getPathsAndPropertiesOf};
