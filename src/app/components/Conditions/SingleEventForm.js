var React = require('react');
var History = require('react-router').History;
var DeepLinkedStateMixin = require('react-deep-link-state');
var DataGenerator = require('../../services/DataGenerator.js');
var SmartSelect = require('../SmartSelect.js');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');
var {getPathsAndPropertiesOf} = require('./common.js');

var SingleEventForm= React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History, DeepLinkedStateMixin],

  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    let Condition = this.props.Condition;
    //let hasLabelOrThing = Condition.labelName || Condition.thingName;
    console.log('getInitialState: Condition: ', Condition);
    return {
      Condition: Condition,
      labelOrThing: '',
      paths: [],
      properties: [],
      operators: [],
      callback: this.props.Callback,
      showValidationErrors: false,
    };
  },
  initializeFields:function(initValue) {
    console.log('initializeFields started');

    let resPromise = getPathsAndPropertiesOf(initValue.labelName, initValue.thingName);
    resPromise.then((res) => {
      _.extend(this.state.Condition, initValue);
      this.setState({
        properties: res.properties,
        paths: res.paths,
        labelOrThing: initValue.labelName ? 'Label' : 'Thing',
      });
      console.log('initializeFields done');
    });
  },
  componentWillReceiveProps:function(nextProps){
    console.log(nextProps);
    if(this.props.Condition != nextProps.Condition){
      console.log('componentWillReceiveProps, Condition', nextProps.Condition);
      this.initializeFields(_.cloneDeep(nextProps.Condition));
      this.setState({Condition:nextProps.Condition});
    }
    console.log(this.state.Condition);
  },
  componentDidMount:function() {
    this.conditionPopulator.getOperators((result) => {
      this.setState({operators: result});


      if (this.props.data) {
        this.initializeFields(this.props.data);
      }
    });
  },

  populateByLabelOrThing:function(labelOrThing) {
    let update = (paths) => {
      console.log('updating paths', paths);
      delete this.state.Condition['labelName'];
      delete this.state.Condition['thingName'];
      this.state.paths = paths;
      this.state.properties = {};
      this.forceUpdate();
    };

    if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabels(update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThings(update);
    } else {
      update([]);
    }

  },

  populateByPath:function(labelOrThing, path) {
    let update = (properties) => {
      this.state.Condition.property = '';
      this.state.properties = properties;
      this.forceUpdate();
    };

    if (!path) {
      update([]);
    } else if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabelProperties(path, update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThingProperties(path, update);
    } else {
      update([]);
    }
  },

  condLabelOrThingChanged:function(v) {
    this.state.labelOrThing = v;
    this.populateByLabelOrThing(v);
    this.forceUpdate();
  },

  condPathChanged:function(v) {
    let condData = this.state.Condition;
    delete condData['labelName'];
    delete condData['thingName'];
    if (this.state.labelOrThing === 'Label') {
      condData.labelName = v;
    } else if (this.state.labelOrThing === 'Thing') {
      condData.thingName = v;
    }
    this.populateByPath(this.state.labelOrThing, v);
    this.forceUpdate();
  },
  updateField:function(field, v) {
    this.state.Condition[field] = v;
    this.forceUpdate();
  },

  SaveCondition:function(e){
    e.preventDefault();
    if (!this.isFormValid()) {
      this.setState({showValidationErrors: true});
      return;
    }
    console.log('==== ', JSON.stringify(this.state.Condition));
    localStorage.setItem('tempCondition', JSON.stringify(this.state.Condition));
    this.state.callback();
    //this.history.goBack();
  },
  isFormValid() {
    let d = this.state.Condition;
    return (d.labelName || d.thingName) &&
           d.property && d.operator && d.operand;
  },


  render: function () {
    let condData = this.state.Condition;
    let isLabel = this.state.labelOrThing === 'Label';
    let isThing = this.state.labelOrThing === 'Thing';
    let pathSelected = condData.labelName && isLabel || condData.thingName && isThing;
    let pSensor = this.state.properties.sensor || [];
    let pState = this.state.properties.state || [];
    let properties = [];
    if (pathSelected) {
      properties.push({
        label: 'Readings',
        items: pSensor.map(p => {
          return {label: p, value: p};
        })
      });
      if (isThing) {
        properties.push({
          label: 'Devices',
          items: pState.map(p => {
            return {label: p, value: p};
          })
        });
      }
    }

    return (
      <fieldset>
        <div className="form-group">
          <div className="input-inline-gp">
            <div className="col-sm-3">
              <SmartSelect
                value={this.state.labelOrThing}
                onSetValue={this.condLabelOrThingChanged}
                firstEmpty={true}
                items={[
                    {label: 'Thing ID', value: 'Thing'},
                    {label: 'Label ID', value: 'Label'},
                  ]} />
            </div>
            <div className="col-sm-4">
              <SmartSelect
                disabled={!isLabel && !isThing}
                firstEmpty={true}
                value={isLabel ? condData.labelName : condData.thingName}
                onSetValue={this.condPathChanged}
                items={
                  this.state.paths.map(p => {
                    return {label: p, value: p};
                  })
                } />
            </div>
          </div>
          <div className="clearfix"></div>
          <div className="input-inline-gp">
            <label className="col-sm-1">Property <sup>*</sup></label>
            <div className="col-sm-3">
              <SmartSelect
                value={condData.property}
                onSetValue={this.updateField.bind(this, 'property')}
                groupedItems={properties} />
            </div>
          </div>
          <div className="clearfix"></div>
          <div className="input-inline-gp">
            <label className="col-sm-1">Operator <sup>*</sup></label>
            <div className="col-sm-2">
              <SmartSelect
                value={condData.operator}
                onSetValue={this.updateField.bind(this, 'operator')}
                disabled={!pathSelected}
                items={
                  this.state.operators.map(o => {
                    return {label: o, value: o}
                  })
                } />
            </div>

            <label className="col-sm-1">Operand <sup>*</sup></label>
            <div className="col-sm-2">
              <input type="text" placeholder="Enter Value" className="wauto" valueLink={this.deepLinkState(['Condition', 'operand'])}/>
            </div>
          </div>

            {
              !this.state.showValidationErrors ||
              this.isFormValid() ? null :
                <div className="form-error-message">
                  All field are mandatory
                </div>
            }
        </div>
        <div className="form-actions">
          <p style={{display:'inline-block'}}><small><sup>*</sup> - fields are mandatory</small></p>
          <button type="submit" className="btn btn-red pull-right" onClick={this.SaveCondition}>Add</button>
        </div>
      </fieldset>



    );
  },


});

module.exports = SingleEventForm;
