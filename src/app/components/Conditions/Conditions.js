var React = require('react');
var History = require('react-router').History;

var Conditions = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],
  
  

  render: function () {
	return (
	<div>
	 {this.props.children}
	 </div>
	);
  },


});

module.exports = Conditions;