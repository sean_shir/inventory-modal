var React = require('react');
var History = require('react-router').History;
var DataGenerator = require('../../services/DataGenerator.js');
var DeepLinkedStateMixin = require('react-deep-link-state');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var SmartSelect = require('../SmartSelect.js');
var _ = require('lodash');
var {getPathsAndPropertiesOf} = require('./common.js');


var SequenceForm= React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History, DeepLinkedStateMixin],

  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    let seqConds = this.props.data.eventFlow.events[0].conditions.map(() =>
      this.createEmptyCond()
    );
    return {
      ConditionData:this.props.data,
      initCond: {
        labelOrThing: '',
        fields: {
          paths: [],
          properties: {},
        }
      },
      seqConds,
      operators: [],
    };
  },
  createEmptyCond:function() {
    return {
      labelOrThing: '',
      fields: {
        paths: [],
        //properties: {},
      }
    };
  },
  componentWillReceiveProps:function(nextProps){
    console.log(nextProps)
      if(this.props.data != nextProps.data){
        this.initializeFields(_.cloneDeep(nextProps.data));
        this.setState({ConditionData:nextProps.data, showValidationErrors: false});
      }
  },
  componentDidMount:function() {
    this.conditionPopulator.getOperators((result) => {
      this.setState({operators: result});


      if (this.props.data) {
        this.initializeFields(this.props.data);
      }
    });
  },

  initializeFields:function(initConditionData) {
    console.log('SequenceForm, initializeFields started');
    if (!initConditionData.initialCondition.labelName &&
        !initConditionData.initialCondition.thingName) {
          return;
    }
    let conditions = initConditionData.eventFlow.events[0].conditions;

    let promises = conditions.map(c => {
      return getPathsAndPropertiesOf(c.labelName, c.thingName);
    });

    let newSeqConds;

    Promise
      .all(promises)
      .then(results => {
        newSeqConds = results.map((r,i) => {
          return {
            labelOrThing: conditions[i].labelName ? 'Label' : 'Thing',
            fields: {
              paths: r.paths,
              properties: r.properties,
            },
          };
        });

        return getPathsAndPropertiesOf(initConditionData.initialCondition.labelName,
        initConditionData.initialCondition.thingName);
      })
      .then(result => {
        console.log('SequenceForm promises resolved: ', newSeqConds, result);
        _.extend(this.state.ConditionData, initConditionData);
        console.log('SequenceForm new ConditionData: ', this.state.ConditionData);

        this.setState({
          initCond: {
            labelOrThing: initConditionData.initialCondition.labelName ? 'Label' : 'Thing',
            fields: {
              paths: result.paths,
              properties: result.properties,
            }
          },
          seqConds: newSeqConds,
        });
      });
  },

  isFormValid:function() {
    let id = this.state.ConditionData.initialCondition;
    let initCondValid = (id.labelName || id.thingName) &&
                        id.property && id.operator && id.operand;
    let ds = this.state.ConditionData.eventFlow.events[0].conditions;
    let seqsValid = _.every(ds, d => {
      return (d.labelName || d.thingName) &&
             d.operator &&
             ((d.checkProperty && d.property) ||
              (!d.checkProperty && d.operand));
    });
    console.log('SequenceForm.isFormValid: initial condition ' + (initCondValid ? 'valid' : 'invalid') + ', other conditions ' + (seqsValid ? 'valid' : 'invalid') + ',  here is the this.state.ConditionData: ' + JSON.stringify(this.state.ConditionData));
    return initCondValid && seqsValid;
  },

  addConditions:function(){
    let ConditionData = this.state.ConditionData;
    let newCond = {
      checkProperty: true,
      property: '',
      operand: '',
    };
    ConditionData.eventFlow.events[0].conditions.push(newCond);
    this.state.seqConds.push(this.createEmptyCond());
    this.setState({ConditionData:ConditionData})
  },
  removeCondition:function(i) {
    let {ConditionData, seqConds} = this.state;
    ConditionData.eventFlow.events[0].conditions.splice(i, 1);
    seqConds.splice(i, 1);
    this.forceUpdate();
  },
  changePEvent:function(i, v){
    console.log('!!! ', i, v)
    this.state.ConditionData.eventFlow.events[0].conditions[i].checkProperty = v;
    this.forceUpdate();
  },
  updateSequence:function(index,field,v){
    console.log(v,index,field);
    var condition= this.state.ConditionData;
    condition.eventFlow.events[0].conditions[index][field] = v;
    this.setState({ConditionData:condition});
    console.log(this.state.ConditionData)
  },

  populateByLabelOrThing:function(cond, condData, labelOrThing) {
    let update = (paths) => {
      console.log('updating paths', paths);
      delete condData['labelName'];
      delete condData['thingName'];
      cond.fields = {paths, properties: {}};
      //this.populateByPath(cond, condData, labelOrThing, ');
      this.forceUpdate();
    };

    if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabels(update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThings(update);
    } else {
      update([]);
    }

  },

  populateByPath:function(cond, condData, labelOrThing, path) {
    let update = (properties) => {
      condData.property = '';
      cond.fields.properties = properties;
      this.forceUpdate();
    };

    if (!path) {
      update([]);
    } else if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabelProperties(path, update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThingProperties(path, update);
    } else {
      update([]);
    }
  },


  initCondLabelOrThingChanged:function(v) {
    console.log('initCondLabelOrThingChanged: ', v);
    this.state.initCond.labelOrThing = v;
    this.populateByLabelOrThing(this.state.initCond,
                                this.state.ConditionData.initialCondition,
                                v);
    this.forceUpdate();
  },

  initCondPathChanged:function(v) {
    let {initialCondition} = this.state.ConditionData;

      delete initialCondition['labelName'];
      delete initialCondition['thingName'];
    if (this.state.initCond.labelOrThing === 'Label') {
      initialCondition.labelName = v;
    } else if (this.state.initCond.labelOrThing === 'Thing') {
      initialCondition.thingName = v;
    }
    this.populateByPath(this.state.initCond,
                        initialCondition,
                        this.state.initCond.labelOrThing, v);
    this.forceUpdate();
  },
  initCondPropertyChanged:function(v) {
    this.state.ConditionData.initialCondition.property = v;
    this.forceUpdate();
  },
  initCondOperatorChanged:function(v) {
    this.state.ConditionData.initialCondition.operator = v;
    this.forceUpdate();
  },
  initCondOperandChanged:function(e) {
    this.state.ConditionData.initialCondition.operand = e.target.value;
    this.forceUpdate();
  },

  seqCondLabelOrThingChanged:function(i, v) {
    let condData = this.state.ConditionData.eventFlow.events[0].conditions[i];
    let initCondData = this.state.ConditionData.initialCondition;
    let cond = this.state.seqConds[i];

    cond.labelOrThing = v;
    if (v && !condData.property && !condData.operand) {
      condData.property = initCondData.property;
      condData.operand = initCondData.operand;
    }
    this.populateByLabelOrThing(cond, condData, v);
    this.forceUpdate();
  },
  seqCondPathChanged: function(i, isLabel, v) {
    let c = this.state.ConditionData.eventFlow.events[0].conditions[i];
    delete c['labelName'];
    delete c['thingName'];
    if (isLabel) c.labelName = v;
    if (!isLabel) c.thingName = v;
  },

  render: function () {
    let initCondData = this.state.ConditionData.initialCondition;
    let initCond = this.state.initCond;
    let initIsLabel = initCond.labelOrThing === 'Label';
    let initIsThing = initCond.labelOrThing === 'Thing';
    let initPathSelected = initCondData.labelName && initIsLabel || initCondData.thingName && initIsThing;
    let pSensor = initCond.fields.properties.sensor || [];
    let pState = initCond.fields.properties.state || [];
    let initCondProperties = [];
    if (initPathSelected) {
      initCondProperties.push({
        label: 'Readings',
        items: pSensor.map(p => {
          return {label: p, value: p};
        })
      });
      if (initIsThing) {
        initCondProperties.push({
          label: 'Devices',
          items: pState.map(p => {
            return {label: p, value: p};
          })
        });
      }
    }

    var SqueenceHTML = this.state.ConditionData.eventFlow.events[0].conditions.map(function(item,i){

      let cond = this.state.seqConds[i];
      let isLabel = cond.labelOrThing === 'Label';
      let isThing = cond.labelOrThing === 'Thing';
      let pathSelected = item.labelName && isLabel || item.thingName && isThing;

      return(
        <div className="Aggregation_list AggregatinListNumber" key={i}>
          <div className="input-inline-gp">
            <div className="col-sm-3">
              <SmartSelect
                value={cond.labelOrThing}
                onSetValue={this.seqCondLabelOrThingChanged.bind(this,i)}
                firstEmpty={true}
                items={[
                    {label: 'Thing ID', value: 'Thing'},
                    {label: 'Label ID', value: 'Label'},
                  ]} />
            </div>
            <div className="col-sm-4">
              <SmartSelect
                disabled={!cond.labelOrThing}
                value={isLabel ? item.labelName : item.thingName}
                onSetValue={this.seqCondPathChanged.bind(this,i, isLabel)}
                items={
                  cond.fields.paths.map(p => {
                    return {label: p, value: p};
                  })
                } />
            </div>
            <div className="col-sm-1">
              <SmartSelect
                value={item.operator}
                disabled={!pathSelected}
                onSetValue={this.updateSequence.bind(this, i, 'operator')}
                items={
                  this.state.operators.map(o => {
                    return {label: o, value: o}
                  })
                } />
            </div>

          </div>
          <div className="clearfix"></div>
          <div className="input-inline-gp">
            <div className="col-sm-4 text-right" style={{paddingTop:'26px'}}>
              Which is of previous event's
            </div>
            <div className="previousEventBox col-sm-6">
              <div className="radio-right">
                <input type="radio" id={'property_'+i} name={"previousevent_"+i} value="0"
                  checked={item.checkProperty}
                  onChange={this.changePEvent.bind(this,i,true)}  />
                <label style={{width:'95px',marginRight:'10px'}} htmlFor={'property_'+i}>property</label>
                <SmartSelect
                  value={item.property}
                  className={!item.checkProperty ? 'hide preevent': 'wauto'}
                  onSetValue={this.updateSequence.bind(this,i,'property')}
                  groupedItems={!cond.labelOrThing ? [] : initCondProperties} />
              </div>
              <div className="radio-right">
                <input
                  type="radio" id={'value_'+i} name={"previousevent_"+i} value="1"
                  checked={!item.checkProperty}
                  onChange={this.changePEvent.bind(this,i,false)} />
                <label  style={{width:'95px',marginRight:'10px'}} htmlFor={'value_'+i}>value</label>
                <input type="text" placeholder="12.3"
                  value={item.operand}
                  className={item.checkProperty ? 'hide wauto': 'wauto'}
                  onChange={(e) => this.updateSequence(i,'operand', e.target.value)}/>
              </div>
            </div>
            {
              i === 0 ? null :
                <div className="col-sm-3 pull-right">
                  <button type="button" className="btn btn-red" onClick={this.removeCondition.bind(this, i)}>Remove</button>
                </div>
            }
          </div>
        </div>

      )
    },this);

    return (
      <div>
        <fieldset className="col-sm-12">
          <legend>Initial Condition:</legend>
          <div className="form-group" style={{width:'100%'}}>
            <div className="form-inline">
              <div className="Aggregation_list" >
                <div className="input-inline-gp">
                  <div className="col-sm-3">
                    <SmartSelect
                      value={initCond.labelOrThing}
                      onSetValue={this.initCondLabelOrThingChanged}
                      firstEmpty={true}
                      items={[
                        {label: 'Thing ID', value: 'Thing'},
                        {label: 'Label ID', value: 'Label'}
                      ]} />
                  </div>
                  <div className="col-sm-4">
                    <SmartSelect
                      disabled={!initCond.labelOrThing}
                      firstEmpty={true}
                      value={initIsLabel ? initCondData.labelName : initCondData.thingName}
                      onSetValue={this.initCondPathChanged}
                      items={initCond.fields.paths.map(p => {
                          return {label: p, value: p};
                        })} />
                  </div>

                  <div className="col-sm-1">
                    <SmartSelect
                      value={initCondData.operator}
                      onSetValue={this.initCondOperatorChanged}
                      disabled={!initPathSelected}
                      items={
                        this.state.operators.map(o => {
                          return {label: o, value: o}
                        })
                      } />
                  </div>
                  <div className="col-sm-2">
                    <input type="text"
                      value={initCondData.operand}
                      onChange={this.initCondOperandChanged} />
                  </div>

                </div>
                <div className="clearfix" ></div>
                <div className="input-inline-gp">
                  <div className="col-sm-4">
                    <SmartSelect
                      value={initCondData.property}
                      onSetValue={this.initCondPropertyChanged}
                      groupedItems={initCondProperties} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </fieldset>
        <fieldset className="col-sm-12">
          <legend>Sequence Condition:</legend>
          <div className="form-group" style={{width:'100%'}}>
            <div className="form-inline">
              {SqueenceHTML}
            </div>
          </div>
        </fieldset>
        <div className="col-sm-3 pull-right text-right">
          <button type="button" className="btn btn-red" onClick={this.addConditions}>Add Conditions</button>
        </div>
      </div>
    );
  },


});

module.exports = SequenceForm;
