var React = require('react');
var History = require('react-router').History;
var SmartSelect = require('../SmartSelect.js');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');
var {getPathsAndPropertiesOf} = require('./common.js');

var AggregationForm= React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],

  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    let conds = this.props.data.eventFlow.events[0].conditions.map(() =>
      this.createEmptyCond()
    );
    return {
      ConditionData:this.props.data,
      operators: [],
      functions: [],
      conds,
    };
  },
  createEmptyCond:function() {
    return {
      labelOrThing: '',
      fields: {
        paths: [],
        properties: {},
      }
    };
  },
  componentWillReceiveProps:function(nextProps){
    if(this.props.data != nextProps.data){
      this.initializeFields(_.cloneDeep(nextProps.data));
      this.setState({ConditionData:nextProps.data, showValidationErrors: false});
    }
  },

  initializeFields:function(initConditionData) {
    console.log('AggregationForm, initializeFields started');
    let conditions = initConditionData.eventFlow.events[0].conditions;
    if (!conditions[0].thingName && !conditions[0].labelName) {
      console.log('AggregationForm, initializeFields skipped');
      return;
    }

    let promises = conditions.map(c => {
      return getPathsAndPropertiesOf(c.labelName, c.thingName);
    });
    Promise.all(promises).then(results => {
      console.log('AggregationForm promises resolved: ', results);
      //let newConds = _.range(events.count).map(x => this.createEmptyCond());
      _.extend(this.state.ConditionData, initConditionData);
      console.log('AggregationForm new ConditionData: ', this.state.ConditionData);
      let newConds = results.map((r,i) => {
        return {
          labelOrThing: conditions[i].labelName ? 'Label' : 'Thing',
          fields: {
            paths: r.paths,
            properties: r.properties,
          },
        };
      });
      this.setState({conds: newConds});
    });
  },

  componentDidMount:function() {
    this.conditionPopulator.getOperators((operators) => {
      this.conditionPopulator.getFunctions((functions) => {
        this.setState({operators, functions});

        if (this.props.data) {
          this.initializeFields(this.props.data);
        }
      });
    });
  },

  isFormValid:function() {
    let ds = this.state.ConditionData.eventFlow.events[0].conditions;
    return _.every(ds, (d,i) => {
      return (d.labelName || d.thingName) &&
             d.property && d.operator && d.operand &&
             (i > 0 || d.function);
    });
  },

  addConditions:function(){
    let ConditionData = this.state.ConditionData;
    ConditionData.eventFlow.events[0].conditions.push({"thingName": "","dataType": "","property": "","operator": "","operand": ""});
    this.state.conds.push(this.createEmptyCond());
    this.setState({ConditionData:ConditionData});
  },
  removeCondition:function(index) {
    let ConditionData = this.state.ConditionData;
    ConditionData.eventFlow.events[0].conditions.splice(index, 1);
    this.state.conds.splice(index, 1);
    this.forceUpdate();
  },
  updateSequence:function(index,field,v){
    console.log(v,index,field);
    var condition= this.state.ConditionData;
    condition.eventFlow.events[0].conditions[index][field] = v;
    this.setState({ConditionData:condition});
    console.log(this.state.ConditionData)
  },

  populateByLabelOrThing:function(cond, condData, labelOrThing) {
    let update = (paths) => {
      console.log('updating paths', paths);
      delete condData['thingName'];
      delete condData['labelName'];
      cond.fields = {paths, properties: {}};
      //this.populateByPath(cond, condData, labelOrThing, ');
      this.forceUpdate();
    };

    if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabels(update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThings(update);
    } else {
      update([]);
    }

  },

  populateByPath:function(cond, condData, labelOrThing, path) {
    let update = (properties) => {
      condData.property = '';
      cond.fields.properties = properties;
      this.forceUpdate();
    };

    if (!path) {
      update([]);
    } else if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabelProperties(path, update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThingProperties(path, update);
    } else {
      update([]);
    }
  },

  condLabelOrThingChanged:function(i, v) {
    this.state.conds[i].labelOrThing = v;
    this.populateByLabelOrThing(this.state.conds[i],
                                this.state.ConditionData.eventFlow.events[0].conditions[i],
                                v);
    this.forceUpdate();
  },

  condPathChanged:function(i, v) {
    let cond = this.state.conds[i];
    let condData = this.state.ConditionData.eventFlow.events[0].conditions[i];

    delete condData['thingName'];
    delete condData['labelName'];
    if (cond.labelOrThing === 'Label') {
      condData.labelName = v;
    } else if (cond.labelOrThing === 'Thing') {
      condData.thingName = v;
    }
    this.populateByPath(cond, condData, cond.labelOrThing, v);
    this.forceUpdate();
  },

  render: function () {
    var AggregationHTML = this.state.ConditionData.eventFlow.events[0].conditions.map(function(item,i){

      let cond = this.state.conds[i];
      let isLabel = cond.labelOrThing === 'Label';
      let isThing = cond.labelOrThing === 'Thing';
      let pathSelected = item.labelName && isLabel || item.thingName && isThing;
      let pSensor = cond.fields.properties.sensor || [];
      let pState = cond.fields.properties.state || [];
      let properties = [];
      if (pathSelected) {
        properties.push({
          label: 'Readings',
          items: pSensor.map(p => {
            return {label: p, value: p};
          })
        });
        if (isThing) {
          properties.push({
            label: 'Devices',
            items: pState.map(p => {
              return {label: p, value: p};
            })
          });
        }
      }

      return(

        <div className="Aggregation_list" key={i}>
          <div className="input-inline-gp">
            <div className="col-sm-3">
              <SmartSelect
                value={cond.labelOrThing}
                firstEmpty={true}
                onSetValue={this.condLabelOrThingChanged.bind(this,i)}
                items={[
                    {label: 'Thing ID', value: 'Thing'},
                    {label: 'Label ID', value: 'Label'},
                  ]} />
            </div>
            <div className="col-sm-4">
              <SmartSelect
                disabled={!cond.labelOrThing}
                firstEmpty={true}
                value={isLabel ? item.labelName : item.thingName}
                onSetValue={this.condPathChanged.bind(this,i)}
                items={
                  cond.fields.paths.map(p => {
                    return {label: p, value: p};
                  })
                } />
            </div>
            <div className="col-sm-1">
              <SmartSelect
                value={item.operator}
                onSetValue={this.updateSequence.bind(this, i, 'operator')}
                disabled={!pathSelected}
                items={
                  this.state.operators.map(o => {
                    return {label: o, value: o}
                  })
                } />
            </div>
            <div className="col-sm-2">
              <input value={item.operand} type="text" onChange={(e) => this.updateSequence(i,'operand', e.target.value)} />
            </div>
          </div>
          <div className="clearfix"></div>
          <div className="input-inline-gp">
            <div className={i == 0?'col-sm-3':'hide'}>
              {
                i > 0 ? null :
                  <SmartSelect
                    value={item.function}
                    onSetValue={this.updateSequence.bind(this, i, 'function')}
                    disabled={!pathSelected}
                    items={
                      _.keys(this.state.functions).map(k => {
                        return {label: k, value: this.state.functions[k]}
                      })
                    } />
              }
            </div>
            <div className="col-sm-4">
              <SmartSelect
                value={item.property}
                onSetValue={this.updateSequence.bind(this, i, 'property')}
                groupedItems={properties} />
            </div>
            {
              i === 0 ? null :
                <div className="col-sm-3">
                  <button type="button" className="btn btn-red" onClick={this.removeCondition.bind(this, i)}>Remove</button>
                </div>
            }
          </div>
        </div>

      )
    },this);

    return (
      <fieldset className="col-sm-12">
        <legend>Aggregation:</legend>
        <div className="form-group" style={{width:'100%'}}>
          <div className="form-inline">

            {AggregationHTML}

            <div className="col-sm-3 pull-right text-right">
              <button type="button" className="btn btn-red" onClick={this.addConditions}>Add Conditions</button>
            </div>
          </div>
        </div>
      </fieldset>

    );
  },


});

module.exports = AggregationForm;
