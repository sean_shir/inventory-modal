var React = require('react');
var History = require('react-router').History;
var DeepLinkedStateMixin = require('react-deep-link-state');
var SensorServ = require('../../services/SensorService.js'); 
var DataGenerator = require('../../services/DataGenerator.js'); 
var CreateSensor= React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History, DeepLinkedStateMixin],
  
  getInitialState: function() {
      return {
      	sensor:[],
      	error:[]
      };
    },
  componentDidMount: function() {
  	this.getSensor();
  },
  getSensor:function(){
  	var id = this.props.params.SensorId;
  	var self = this;
  	if(id != undefined){
  		SensorServ.GetSensorDetail(id).then(function(data){
	        if (self.isMounted()) {
		      	self.setState({sensor:data})
		  	}	
		})
	  }
	  else{
  		var data  = DataGenerator.GenerateSensorData()
  		self.setState({sensor:data})
	  }
  },
  saveSensor:function(e){
  	var self = this;
  	e.preventDefault();
  	
  	if(this.props.params.SensorId != undefined){
  		SensorServ.UpdateSensor(this.props.params.SensorId,this.state.sensor).then(function(response){
  			self.history.pushState(null, "/Sensor/");
  		})
  	}
  	else{
  		if(this.state.sensor.name!="")
  		{
	  		SensorServ.AddSensor(this.state.sensor).then(function(response){
	  			self.history.pushState(null,"/Sensor/")
	  		});
	  	}
	  	else
	  	{
	  		var error = this.state.error
	  		error.push("name can't be empty");
	  		this.setState({error:error})
	  	}
  	}
  },

  goBack:function(){
  	this.history.pushState(null,"/Sensor/")
  },
 
 
  render: function () {
     var error = this.state.error.map(function(item,i){
    	return (
    		<p key={i}>{item}</p>
    		
    		)
    })
return (
	<div>
	<div className="create-state create-rule">

		<div className="page-header">

			<h2>+ Create Sensor Type</h2>
			<div className="page-header-link pull-right">
				<a onClick={this.goBack} className="btn btn-red btn-icon"><i className="fa fa-angle-left btn-left-icon"></i> Back</a>
			</div>
		</div>
		<div className="page-content">
			<div className={error.length > 0? 'alert alert-danger col-sm-6':'hide'}>
				{error}
			</div>
			<form>
				<div className="form-group">
					<label>Name<sup>*</sup></label>
					<input type="text" placeholder="Enter Name (lowercase)" className="wauto" valueLink={this.deepLinkState(['sensor', 'name'])}/>
				</div>
				
				<div className="form-group">
					<p><label onClick={this.test}>Description</label></p>
					<p><textarea  placeholder="please enter the description (optional)" valueLink={this.deepLinkState(['sensor', 'description'])}></textarea></p>
				</div>
				
				<div className="form-actions">
					<p style={{display:'inline-block'}}><small><sup>*</sup> - fields are mandatory</small></p>
					<button type="submit" className="btn btn-red pull-right" onClick={this.saveSensor}>Save</button>
				</div>
			</form>
		</div>
	</div>
	</div>
  );
},


});

module.exports = CreateSensor;