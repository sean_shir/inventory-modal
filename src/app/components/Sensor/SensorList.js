var React = require('react');
var History = require('react-router').History;
var Modal = require('../core/Modal.js');


var SensorList = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],

  getInitialState: function() {
      return {
        SensorList: [],
        removeid:''
      };
    },
  componentDidMount: function() {
  	$.get('http://localhost:3004/sensors', function(result) {
            var collection = result.content ? result.content : result;
            if (this.isMounted()) {
              this.setState({
                SensorList: collection
              });
            }
            console.log('This are the sensor list:', JSON.stringify(collection[0]))
          }.bind(this));
  },
  deleteSensor:function(i){
  	this.setState({removeid:i})
  	this.refs.RemoveSensor.show()
  },
  removeSensor:function(){
  	//var statelist = this.state.StateList;
  	var id = Number(this.state.removeid)
    var self = this;
    console.log(this.state.removeid)
  	//statelist.splice(id, 1);
    $.ajax({
      url: 'http://localhost:3004/sensors/'+id,
      type: 'DELETE',
      success: function(result) {
          $.get('http://localhost:3004/sensors', function(result) {
            var collection = result;
            if (self.isMounted()) {
              self.setState({
                SensorList: collection
              });
            }
          }.bind(this));
      }
  });
  	//this.setState({StateList:statelist})
    this.refs.RemoveSensor.hide();
  },
   HideModal:function(){
    this.refs.RemoveSensor.hide()
  },
  render: function () {
    var List  = this.state.SensorList.map(function(item, i){
    	return(
    			<li key={i} className="list-group-item">
    				<div className="list-details">
					    <h4 className="list-group-item-heading">Sensor Name: {item.name}</h4>
					    <p className="list-group-item-text">Description: {item.description}</p>
					 </div>
					 <div className="list-action">
					 	<a href={'#/EditSensor/'+item.id} className="btn btn-red"><i className="fa fa-pencil"></i></a>
					 	<button type="button" className="btn btn-red" onClick={this.deleteSensor.bind(this,item.id)}><i className="fa fa-trash"></i></button>
					 </div>
				  </li>
    		)
    },this)

     
let customActions = [
      <a key="1" className="btn-red btn" onClick={this.removeSensor}>Yes</a>,
      <a key="2" className="btn btn-default " onClick={this.HideModal}>No</a>
    ]

return (
<div>
 <div className="page-header">
	<h2>List of Sensor Types</h2>
	<div className="page-header-link pull-right">
		<a href="#/CreateSensor" className="btn btn-red btn-icon"><i className="fa fa-star btn-left-icon"></i> Add Sensor &nbsp; <i className="fa fa-angle-right btn-right-icon"></i></a>
	</div>
</div>
<div className="page-content">
 <ul className="list-group">
  {List}
</ul>
</div>
  <Modal ref="RemoveSensor" id="RemoveSensor" title="Add Sensor" actions={customActions}>
      Do you want to delete this Sensor.
    </Modal>
	
</div>

  );
},
});

module.exports = SensorList;
