var React = require('react');
var History = require('react-router').History;
var Modal = require('../core/Modal.js');
var DeepLinkedStateMixin = require('react-deep-link-state');

var RuleServ = require('../../services/RuleService.js');

var RuleList = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History, DeepLinkedStateMixin],

  getInitialState: function() {
      return {
        RuleList: [],
        removeid:''
      };
    },
  componentDidMount: function() {
    var self = this;
    RuleServ.GetRuleList().then(function(data){
      console.log('RuleList.componentDidMount, GetRuleList, data: ', data);
      self.setState({RuleList:data})
    })
  },
  deleteRule:function(i){
    console.log('deleteRule: ' + i);
    this.setState({removeid:i})
    this.refs.RemoveRule.show();
  },
  removeRule:function(){
    console.log('removeRule, removeid: ' + this.state.removeid, ', RuleList: ', this.state.RuleList);
    var ruleList = this.state.RuleList;
    var id = Number(this.state.removeid);
    var self = this;
    console.log(id);
    RuleServ.DeleteRule(id).then(function(response){
      console.log('removeRule, response of DeleteRule: ', response);
      self.refs.RemoveRule.hide();
      RuleServ.GetRuleList().then(function(data){
        console.log('removeRule, response of GetRuleList: ' + data);
        self.setState({RuleList:data})
      })
    })

  },
  HideModal:function(){
    this.refs.RemoveRule.hide()
  },
  updateStatus:function(index, e){
    var RuleList = this.state.RuleList;
    RuleList[index].enabled = e.target.checked;
    this.setState({RuleList:RuleList})
    var selectedRule = RuleList[index]
    RuleServ.UpdateRule(selectedRule.id,selectedRule).then(function(response){
       // self.history.pushState(null, "/Rule/");
      })

  },
  SearchRule:function(){
    var searchText = this.refs.search.value;
    var list = this.state.RuleList;
    list = list.filter(function (el) {
          return el.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
      });
    this.setState({RuleList:list})
  },
  render: function () {
    var List  = this.state.RuleList.map(function(item, i){
      return(
          <tr key={i} className={item.enabled? '' : 'disabled'}>
            <td className="rule_name">
              {item.name}
            </td>
            <td className="rule_description">
              {item.description}
            </td>
            <td>

          <div className="onoffswitch">
            <input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id={'myonoffswitch'+i} onChange={this.updateStatus.bind(this,i)} checked={item.enabled} />
            <label className="onoffswitch-label" htmlFor={'myonoffswitch'+i}></label>
        </div>

            </td>
            <td className="rule_actions">
               <a href={'#/editrule/'+item.id} className="btn btn-sm btn-rounded btn-red"><i className="fa fa-pencil"></i></a>
              <a className="btn-icon-c" onClick={this.deleteRule.bind(this,item.id)}><i className="fa fa-trash"></i></a>
            </td>
          </tr>
        )
    },this)

     let customActions = [
      <a key="1" className="btn-red btn" onClick={this.removeRule}>Yes</a>,
      <a key="2" className="btn btn-default " onClick={this.HideModal}>No</a>
    ]


return (
<div>
 <div className="page-header">
  <h2>List of Rules</h2>
  <div className="page-header-search ">
   <div className="col-sm-9"><div className="row"> <i className="fa fa-search"></i><input type="search" className="search" ref="search"/></div></div>
    <button className="btn btn-red btn-sm pull-right" onClick={this.SearchRule}>Search</button>
  </div>
  <div className="page-header-link">
    <a href="#/CreateRule" className="btn btn-red"> + Add a new rule</a>	</div>
</div>
<div className="page-content">
 <table className="rule_list table-primary">
    <thead>
      <tr className="text-center">
        <th>Name</th>
        <th>Description</th>
        <th>Status</th>
        <th>edit/delete</th>
      </tr>
    </thead>
    <tbody>
      {List}
    </tbody>
</table>
</div>

  <Modal ref="RemoveRule" id="RemoveRule" title="Add Actoin" actions={customActions}>
      Do you want to delete this State.
    </Modal>
</div>

  );
},


});

module.exports = RuleList;
