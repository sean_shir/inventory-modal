var React = require('react');
var History = require('react-router').History;
var DeepLinkedStateMixin = require('react-deep-link-state');
var DataGenerator = require('../../services/DataGenerator.js');
var SmartSelect = require('../SmartSelect.js');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');


var ExecuteCommandForm= React.createClass({
  mixins: [History, DeepLinkedStateMixin],
  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    return this.createFreshState();
  },
  createFreshState: function() {
    let data = DataGenerator.GenerateActionData(2);
    let argPairs = _.toPairs(data.args);
    let delay = this.getTimeValueAndUnitFromString(data.Delay);
    console.log(data);
    return {
      Action:data,
      labelOrThing: '',
      paths: [],
      argPairs,
      ...delay,
      timeFormats: {},
    };
  },
  componentDidMount: function() {
    console.log('ExecuteCommandForm.componentDidMount: data', this.props.data);
    let {data} = this.props;
    this.conditionPopulator.getTimeFormats(result => {
      this.setState({timeFormats: result});

      if (data && data.type === 'ExecuteCommand') {
        this.initializeFields(_.cloneDeep(data));
      }
    });
  },
  componentWillReceiveProps:function(nextProps) {
    console.log('ExecuteCommandForm.componentWillReceiveProps: data', nextProps.data);
    if(nextProps.data && this.props.data != nextProps.data && nextProps.data.type === 'ExecuteCommand') {
      this.initializeFields(_.cloneDeep(nextProps.data));
    }
  },
  clear: function() {
    this.setState({
      ...this.createFreshState(),
      timeFormats: this.state.timeFormats,
    });
  },
  initializeFields:function(initData) {
    console.log('ExecuteCommandForm.initializeFields started');
    let update = paths => {
      this.state.Action = _.cloneDeep(initData);
      let delay = this.getTimeValueAndUnitFromString(initData.Delay);
      console.log('ExecuteCommandForm.initializeFields delay: ', delay);
      this.setState({
        paths,
        labelOrThing: initData.labelName ? 'Label' : 'Thing',
        argPairs: _.toPairs(initData.args),
        ...delay,
      });
      console.log('ExecuteCommandForm.initializeFields done');
    };
    if (initData.labelName) {
      this.conditionPopulator.getLabels(update);
    } else if (initData.thingName) {
      this.conditionPopulator.getThings(update)
    }
  },
  getTimeValueAndUnitFromString(s) {
    let match = /(\d+)\s+(.*)/.exec(s);
    if (!match) return {delayTimeValue: 0, delayTimeUnit: '', delayChecked: !!match};
    return {delayTimeValue: match[1], delayTimeUnit: match[2], delayChecked: !!match};
  },
  isFormValid: function() {
    console.log('ExecuteCommandForm.isFormValid: ', this.state.Action);
    let action = this.state.Action;
    return (action.labelName || action.thingName) &&
           action.command &&
           _.every(this.state.argPairs, p => p[0] && p[1]) &&
           (!this.state.delayChecked || action.Delay);
  },
  populateByLabelOrThing:function(labelOrThing) {
    let update = (paths) => {
      console.log('updating paths', paths);
      delete this.state.Action['labelName'];
      delete this.state.Action['thingName'];
      this.state.paths = paths;
      //this.state.properties = {};
      this.forceUpdate();
    };

    if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabels(update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThings(update);
    } else {
      update([]);
    }

  },

  labelOrThingChanged:function(v) {
    this.state.labelOrThing = v;
    this.populateByLabelOrThing(v);
    this.forceUpdate();
  },
  pathChanged:function(v) {
    let action = this.state.Action;
    delete action['labelName'];
    delete action['thingName'];
    if (this.state.labelOrThing === 'Label') {
      action.labelName = v;
    } else if (this.state.labelOrThing === 'Thing') {
      action.thingName = v;
    }
    this.forceUpdate();
  },
  updateArgKey: function(i, e) {
    this.state.argPairs[i][0] = e.target.value;
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },

  updateArgValue: function(i, e) {
    this.state.argPairs[i][1] = e.target.value;
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },
  delayCheckedChanged: function(e) {
    let checked = e.target.checked;
    if (checked) {
      this.updateDelay(this.state.delayTimeValue,
                       this.state.delayTimeUnit);
    } else {
      this.updateDelay(null, null);
    }
    console.log('delayCheckedChanged', e.target.value, checked);
    this.setState({delayChecked: checked});
  },

  delayTimeValueChanged: function(e) {
    let v = Number(e.target.value);
    this.updateDelay(v, this.state.delayTimeUnit);
    this.setState({delayTimeValue: e.target.value});
  },

  delayTimeUnitChanged: function(newUnit) {
    this.updateDelay(this.state.delayTimeValue, newUnit);
    this.setState({delayTimeUnit: newUnit});
  },
  updateDelay: function(v, u) {
    let n = Number(v);
    if (Number.isFinite(n) && n > 0 && u) {
      this.state.Action.Delay = v + ' ' + u;
    } else {
      this.state.Action.Delay = '';
    }
  },
  commandChanged: function(e) {
    this.state.Action.command = e.target.value;
    this.forceUpdate();
  },
  AddAnotherArg:function(){
    this.state.argPairs.push(['', '']);
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },
  removeArg:function(i) {
    this.state.argPairs.splice(i, 1);
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },

  render: function () {
    let action = this.state.Action;
    let isLabel = this.state.labelOrThing === 'Label';

    var ActionArgList = this.state.argPairs.map(function(argPair, i){
      return(
        <div>
          <div className="form-group">
            <label className="col-sm-4">Argument</label>
            <div className="col-sm-5">
              <input type="text"
                value={argPair[0]}
                onChange={this.updateArgKey.bind(this, i)}
                placeholder="key"
                style={{width:'45%'}}
                /> :
              <input type="text"
                value={argPair[1]}
                onChange={this.updateArgValue.bind(this, i)}
                placeholder="value"
                style={{width:'45%'}}
                />
            </div>
            {
              <div className="col-sm-3 pull-right">
                <button type="button"
                  className="btn btn-red"
                  onClick={this.removeArg.bind(this, i)}>
                  Remove
                </button>
              </div>
            }
          </div>
        </div>
      );
    },this);


    return (
      <div className="ExecuteCommandForm bordered addactionform">
        <div className="form-group">
          <div className="col-sm-4">
            <SmartSelect
              value={this.state.labelOrThing}
              firstEmpty={true}
              onSetValue={this.labelOrThingChanged}
              items={[
                  {label: 'Thing ID', value: 'Thing'},
                  {label: 'Label ID', value: 'Label'},
                ]} />
          </div>
          <div className="col-sm-5">
            <SmartSelect
              disabled={!this.state.labelOrThing}
              firstEmpty={true}
              value={isLabel ? this.state.Action.labelName : this.state.Action.thingName}
              onSetValue={this.pathChanged}
              items={
                this.state.paths.map(p => {
                  return {label: p, value: p};
                })
              } />
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-4">Command<sup>*</sup></label>
          <div className="col-sm-5">
            <input value={this.state.Action.command} type="text"
              onChange={this.commandChanged} />
          </div>
        </div>

        <fieldset className=" primary">
          {ActionArgList}
          <div className="col-sm-4" style={{"float":"right"}}>
            <button type="button" className="btn btn-red btn-sm"  onClick={this.AddAnotherArg}>Add an argument</button>
          </div>
        </fieldset>
        <div className="form-group">
          <div className="checkbox col-sm-3">
            <input type="checkbox"
              id="set_state_delay"
              checked={this.state.delayChecked}
              onChange={this.delayCheckedChanged}
            />
            <label htmlFor="set_state_delay">Delay</label>
          </div>
          <div className="col-sm-3">
            <input
              disabled={!this.state.delayChecked}
              value={this.state.delayTimeValue}
              onChange={this.delayTimeValueChanged}
              type="number" />
          </div>
          <div className="col-sm-3">
            <SmartSelect
              disabled={!this.state.delayChecked}
              value={this.state.delayTimeUnit}
              onSetValue={this.delayTimeUnitChanged}
              items={
                _.map(this.state.timeFormats, (v,k) => {
                  return {label: k, value: v};
                })
              } />
          </div>
        </div>
          {
            !this.props.showValidationErrors ||
            this.isFormValid() ? null :
              <div className="form-error-message">
                All field are mandatory
              </div>
          }
      </div>
    );
    /*
    return (
      <div className="ExecuteCommandForm bordered addactionform">
        <div className="form-group">
          <label className="col-sm-4">
            ThingID/Lable <sup>*</sup>
          </label>
          <div className="col-sm-5">
            <select>
              <option>ThingID</option>
              <option>Label</option>
            </select>
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-4">Template <sup>*</sup></label>
          <div className="col-sm-5">

            <select valueLink={this.deepLinkState(['Action', 'template'])}>
              <option value="ThingName1">ThingName1</option>
              <option value="ThingName2">ThingName2</option>
              <option value="ThingName3">ThingName3</option>
              <option value="ThingName4">ThingName4</option>
              <option value="ThingName5">ThingName5</option>
            </select>
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-4">Command<sup>*</sup></label>
          <div className="col-sm-5">
            <select valueLink={this.deepLinkState(['Action', 'command'])}>
              <option value="Command1">Command 1</option>
              <option value="Command2">Command 2</option>
              <option value="Command3">Command 3</option>
              <option value="Command4">Command 4</option>
              <option value="Command5">Command 5</option>
              <option value="Command6">Command 6</option>
            </select>
          </div>
        </div>
        <div className="form-group">
          <label className="col-sm-4">Arguments</label>
          <div className="col-sm-5">
            <input type="text" placeholder="key" style={{width:'45%'}} valueLink={this.deepLinkState(['Action', 'arguments', 'key'])}/> :
      <input type="text" placeholder="value" style={{width:'45%'}} valueLink={this.deepLinkState(['Action', 'arguments', 'value'])}/>
          </div>
        </div>
        <div className="form-group">
          <div className="col-sm-4 checkbox">
            <input type="checkbox" htmlFor="delay"/>
            <label id="delay">Delay</label>
          </div>
          <div className="col-sm-5">
            <input type="text" placeholder="key" style={{width:'30%'}} valueLink={this.deepLinkState(['Action', 'Delay'])} /> :
      <select style={{width:'65%'}}>
        <option>Mili Seconds</option>
        <option>Seconds</option>
      </select>
          </div>
        </div>


      </div>
    );
    */
  },
});

module.exports = ExecuteCommandForm;
