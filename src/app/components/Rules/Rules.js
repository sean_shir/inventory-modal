var React = require('react');
var History = require('react-router').History;

var Rule = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],
  
  

  render: function () {
	return (
	<div>
	 {this.props.children}
	 </div>
	);
  },


});

module.exports = Rule;