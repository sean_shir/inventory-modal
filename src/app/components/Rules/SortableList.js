var React = require('react');
var placeholder = document.createElement("li");
placeholder.className = "placeholder";
var Lists = [];
var List = React.createClass({
  getInitialState: function() {
    return {data: Lists};
  },
  componentWillReceiveProps:function(nextProps) {
    if(nextProps.data != Lists){
      Lists = nextProps.data
    }
  },
  componentWillMount  :function(){
    Lists = this.props.data
  },
  componentDidMount:function(){
    var Lists = this.props.data
    this.setState({data:Lists})
  },
  dragStart: function(e) {
    this.dragged = e.currentTarget;
    e.dataTransfer.effectAllowed = 'move';

    // Firefox requires dataTransfer data to be set
    e.dataTransfer.setData("text/html", e.currentTarget);
  },
  dragEnd: function(e) {

    this.dragged.style.display = "block";
    this.dragged.parentNode.removeChild(placeholder);

    // Update data
    var data = Lists;
    var from = Number(this.dragged.dataset.id);
    var to = Number(this.over.dataset.id);
    if(from < to) to--;
    if(this.nodePlacement == "after") to++;
    data.splice(to, 0, data.splice(from, 1)[0]);
    this.setState({data: data});
  },
  dragOver: function(e) {
    e.preventDefault();
    this.dragged.style.display = "none";
    if(e.target.className == "placeholder") return;
    this.over = e.target;
    // Inside the dragOver method
    var relY = e.clientY - this.over.offsetTop;
    var height = this.over.offsetHeight / 2;
    var parent = e.target.parentNode;

    if(relY > height) {
      this.nodePlacement = "after";
      parent.insertBefore(placeholder, e.target.nextElementSibling);
    }
    else if(relY < height) {
      this.nodePlacement = "before"
      parent.insertBefore(placeholder, e.target);
    }
  },
  EditItem:function(i){
    this.props.onEditClick(i);
  },
  RemoveItem:function(i) {
    this.props.onRemoveClick(i);
  },
  createTitleForCondition:function(item) {
    if (item.type === 'multi') {
      return item.eventFlow.type;
    } else {
      return item.thingName || item.labelName;
    }
  },
  createTitleForAction:function(item) {
    return item.type;
  },
  render: function() {
    if(Lists.length > 0){
      var listItems = Lists.map((item, i) => {
        let title = this.props.type === 'condition' ?
                    this.createTitleForCondition(item) :
                    this.createTitleForAction(item);
        return (
          <li className="sortable-item" data-id={i}
            key={i}
            draggable="true"
            onDragEnd={this.dragEnd}
            onDragStart={this.dragStart}>
            {title}
            <div className="pull-right sortableAction">
              <button type="button" onClick={this.EditItem.bind(this,i)} className="btn btn-sm btn-rounded btn-red"><i className="fa fa-pencil"></i></button>
              <button type="button"  onClick={this.RemoveItem.bind(this,i)} className="btn btn-sm btn-rounded btn-red"><i className="fa fa-minus"></i></button>
            </div>
          </li>
        )
      });
    }

    return (
      <ul onDragOver={this.dragOver}>
        {listItems}
      </ul>
    );
  }
});

module.exports = List;
