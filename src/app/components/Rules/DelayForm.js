var React = require('react');
var History = require('react-router').History;
var DeepLinkedStateMixin = require('react-deep-link-state');
var DataGenerator = require('../../services/DataGenerator.js');
var SmartSelect = require('../SmartSelect.js');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');

var DelayForm= React.createClass({
  mixins: [History, DeepLinkedStateMixin],
  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    return this.createFreshState();

  },
  createFreshState: function() {
    var data = DataGenerator.GenerateActionData(4);
    return {
      Action:data,
      delayTimeValue: 0,
      delayTimeUnit: '',
      timeFormats: {},
    };
  },
  componentDidMount: function() {
    let {data} = this.props;
    this.conditionPopulator.getTimeFormats(result => {
      this.setState({timeFormats: result});

      if (data && data.type === 'Delay') {
        this.initializeFields(_.cloneDeep(data));
      }
    });
  },
  componentWillReceiveProps:function(nextProps) {
    if(nextProps.data && this.props.data != nextProps.data && nextProps.data.type === 'Delay') {
      this.initializeFields(_.cloneDeep(nextProps.data));
    }
  },
  clear: function() {
    this.setState({
      ...this.createFreshState(),
      timeFormats: this.state.timeFormats,
    });
  },
  initializeFields:function(initData) {
    this.setState({
      Action: _.cloneDeep(initData),
    });
  },
  isFormValid: function() {
    let delay = this.state.Action.Delay;
    return delay.time && delay.type;
  },
  delayTimeValueChanged: function(e) {
    this.state.Action.Delay.time = e.target.value;
    this.forceUpdate();
  },

  delayTimeUnitChanged: function(newUnit) {
    this.state.Action.Delay.type = newUnit;
    this.forceUpdate();
  },
  render: function () {
    return (
      <div className="DelayForm bordered addactionform">
        <div className="form-group">
          <label className="col-sm-4">Delay <sup>*</sup></label>
          <div className="col-sm-4">
            <input value={this.state.Action.Delay.time}
              type="number" placeholder="Enter Time"
              onChange={this.delayTimeValueChanged} />
          </div>
          <div className="col-sm-4">
            <SmartSelect
              value={this.state.Action.Delay.type}
              onSetValue={this.delayTimeUnitChanged}
              items={
                _.map(this.state.timeFormats, (v,k) => {
                  return {label: k, value: v};
                })
              } />
          </div>
        </div>


      </div>
    );
  },
});

module.exports = DelayForm;
