var React = require('react');
var History = require('react-router').History;
var ReactTabs = require('react-tabs');
var Tab = ReactTabs.Tab;
var Tabs = ReactTabs.Tabs;
var TabList = ReactTabs.TabList;
var TabPanel = ReactTabs.TabPanel;
var DataGenerator = require('../../services/DataGenerator.js');
var RuleServ = require('../../services/RuleService.js');
var DeepLinkedStateMixin = require('react-deep-link-state');
var _ = require('lodash');


var SortableList = require('./SortableList.js');
var Modal = require('../core/Modal.js');

var CreateSingleCondition = require('../Conditions/CreateSingleCondition.js')
var CreateMultiCondition = require('../Conditions/CreateMultiCondition.js')
var SetStateForm = require('./SetStateForm.js')
var ExecuteCommandForm = require('./ExecuteCommandForm.js')
var TagMessageForm = require('./TagMessageForm.js')
var DelayForm = require('./DelayForm.js')
var PublishMessageForm = require('./PublishMessageForm.js')
var Rule = {}
var CreateRule= React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History, DeepLinkedStateMixin],

  getInitialState: function() {
    return {
      ActionList:[],
      ConditionList:[],
      ActionType:'',
      pagetitle:'Create Rule',
      Rule:{},
      GeneratAction:'',
      editingConditionIndex: -1, // -1 means not editing
      editingActionIndex: -1, // -1 means not editing
      showActionValidationErrors: false,
    };
  },
  componentDidMount: function() {
    this.getRule();
  },
  getRule:function(){
    var id = this.props.params.RuleId;
    var self = this;
    var tempRule = JSON.parse(localStorage.getItem('tempRule') );
    if(tempRule == undefined){
      if(id != undefined){
        RuleServ.GetRuleDetail(id).then(function(data){
          if (self.isMounted()) {
            var TempCondition = JSON.parse( localStorage.getItem('tempCondition') );

            console.log(TempCondition);
            if(TempCondition != undefined){
              data.condition.push(TempCondition)
                localStorage.removeItem('tempCondition');
              console.log(data)
            }
            self.setState({Rule:data,ActionList:data.actions,ConditionList:data.condition});
            localStorage.setItem('tempRule', JSON.stringify(data));
          }
        })
      }
      else{
        var data  = DataGenerator.GenerateRuleData()
          var TempCondition = JSON.parse( localStorage.getItem('tempCondition') );
        console.log(TempCondition);
        if(TempCondition != undefined){
          data.condition.push(TempCondition);
          localStorage.removeItem('tempCondition');
          console.log(data);
        }
        self.setState({Rule:data,ActionList:data.actions,ConditionList:data.condition});
        localStorage.setItem('tempRule', JSON.stringify(data));
      }

    }
    else{
      var data = tempRule;
      var TempCondition = JSON.parse( localStorage.getItem('tempCondition') );
      console.log('TempCondition ', TempCondition);
      if(TempCondition != undefined){
        if (this.state.editingConditionIndex !== -1) {
          data.condition[this.state.editingConditionIndex] = TempCondition;
        } else {
          data.condition.push(TempCondition);
        }
        localStorage.removeItem('tempCondition');
        console.log(data)
      }
      self.setState({Rule:data,ActionList:data.actions,ConditionList:data.condition})
        localStorage.setItem('tempRule', JSON.stringify(data));
      console.log(this.state.Rule)
    }


  },
  saveRule:function(e){
    var self = this;
    e.preventDefault();

    if(this.props.params.RuleId != undefined){
      RuleServ.UpdateRule(this.props.params.RuleId,this.state.Rule).then(function(response){
        self.history.pushState(null, "/Rule/");
      })
    }
    else{
      console.log('saveRule, rule: ', this.state.Rule)
        RuleServ.AddRule(this.state.Rule).then(function(response){
          console.log('saveRule, response of AddRule: ', response);
          self.history.pushState(null,"/Rule/")
        });
    }
    localStorage.removeItem('tempRule')
  },
  editConditionHandler:function(i) {
    console.log('edit ', i);
    if (this.state.ConditionList[i].type === 'single') {
      this.gotoCondtionSingle(i, -1);
    } else {
      this.gotoCondtionMulti(i, -1);
    }
  },
  removeConditionHandler:function(i) {
    console.log('remove ', i);
    this.state.ConditionList.splice(i,1);
    this.forceUpdate();
  },

  gotoCondtionMulti:function(editingConditionIndex, editingActionIndex){
    localStorage.setItem('tempRule', JSON.stringify(this.state.Rule));
    console.log('go to condition multi: ', this.state.Rule);
    this.refs.createMultiCondition.show();
    this.setState({
      editingActionIndex,
      editingConditionIndex,
    });
    //this.history.pushState(null,"/CreateCondition")
  },
  gotoCondtionSingle:function(editingConditionIndex, editingActionIndex){
    localStorage.setItem('tempRule', JSON.stringify(this.state.Rule));
    console.log('go to condition single: ', this.state.Rule);
    this.refs.createSingleCondition.show();
    this.setState({
      editingActionIndex,
      editingConditionIndex,
    });
    //this.history.pushState(null,"/CreateCondition")
  },
  returnFromCSCModel:function(){
    this.getRule();
    this.refs.createSingleCondition.hide();
  },
  returnFromCMCModel:function(page){
    this.getRule();
    this.refs.createMultiCondition.hide();
  },
  goBack:function(){
    localStorage.removeItem('tempRule')
      this.history.pushState(null,"/Rule/")
  },
  test:function(){
    console.log(this.state.Rule)
  },
  editActionHandler:function(i) {
    console.log('edit action ', i);
    this.refs.addAction.show();
    this.setState({
      editingActionIndex: i,
      editingConditionIndex: -1,
      ActionType: this.state.ActionList[i].type,
    });
  },
  removeActionHandler:function(i) {
    console.log('remove action', i);
    this.state.ActionList.splice(i,1);
    this.forceUpdate();
  },
  addAction:function(){
    if (this.refs.ActionForm) {
      this.refs.ActionForm.clear();
    }
    this.refs.addAction.show();
    this.setState({
      editingConditionIndex: -1,
      editingActionIndex: -1,
    });
  },
  SaveAction:function(){
    if (!this.refs.ActionForm.isFormValid()) {
      this.setState({showActionValidationErrors: true});
      return;
    }
    var action = this.refs.ActionForm.state.Action;
    var currentActionList = this.state.ActionList;
    if (this.state.editingActionIndex !== -1) {
      currentActionList[this.state.editingActionIndex] = action;
    } else {
      currentActionList.push(action);
    }
    this.refs.addAction.hide();
    this.setState({ActionList:currentActionList,
                   showActionValidationErrors: false});
    console.log('CreateRule ActionList: ', this.state.ActionList);
  },
  addCondition:function(){
    this.refs.addCondition.show();
  },
  SaveCondition:function(){
    this.refs.addCondition.hide();
    //var action = this.refs.
  },
  setActionType:function(){
    this.setState({ActionType:this.refs.actiontype.value})
  },
  render: function () {
    var actionform ;
    let svf = this.state.showActionValidationErrors;
    let editingAction = this.state.editingActionIndex === -1 ? null :
                        _.cloneDeep(this.state.ActionList[this.state.editingActionIndex]);
                  _.cloneDeep(editingCondition)
    if(this.state.ActionType == 'SetState')
      actionform = <div><SetStateForm data={editingAction}
                          showValidationErrors={svf}
                          ref="ActionForm"/></div>
    else if(this.state.ActionType == 'ExecuteCommand')
      actionform = <div><ExecuteCommandForm data={editingAction}
                          showValidationErrors={svf} ref="ActionForm"/></div>
    else if(this.state.ActionType == 'PublishMessage')
      actionform = <div><PublishMessageForm data={editingAction}
                          showValidationErrors={svf} ref="ActionForm"/></div>
    else if(this.state.ActionType == 'Delay')
      actionform = <div><DelayForm data={editingAction}
                          showValidationErrors={svf} ref="ActionForm"/></div>
    else if(this.state.ActionType == 'TagMessage')
      actionform = <div><TagMessageForm data={editingAction}
                          showValidationErrors={svf} ref="ActionForm"/></div>

    let customActions = [
      <a key="1" className={this.state.ActionType != '' ? 'btn-red btn pull-right' : 'hide'} onClick={this.SaveAction}>Save</a>,
      ];
    let addConditionActions=[
      <a key="1" className="btn-red btn pull-right" onClick={this.SaveCondition}>Save</a>
    ];

    var addActionClsName = "hide";
    if(this.state.ConditionList.length > 0){
      addActionClsName = (this.state.ConditionList[0].type == "single") ? "btn-sm pull-right btn-red btnbtn-sm pull-right btn-red btn" : "hide";
    }

    let editingCondition = this.state.editingConditionIndex === -1 ? null :
                           this.state.ConditionList[this.state.editingConditionIndex];
    console.log('CreateRule, editingCondition', editingCondition);

    return (
      <div className="create-state create-rule">

        <div className="page-header">

          <h2>+ {this.state.pagetitle}</h2>
          <div className="page-header-link pull-right">
            <a onClick={this.goBack} className="btn btn-red btn-icon"><i className="fa fa-angle-left btn-left-icon"></i> Back</a>
          </div>
        </div>
        <div className="page-content">
          <form>
            <div className="form-group">
              <label>Rule Name<sup>*</sup></label>
              <input type="text" placeholder="Enter Name (lowercase)" className="wauto" valueLink={this.deepLinkState(['Rule', 'name'])}/>
            </div>

            <div className="form-group">
              <p><label onClick={this.test}>Description</label></p>
              <p><textarea  placeholder="please enter the description (optional)" valueLink={this.deepLinkState(['Rule', 'description'])}></textarea></p>
            </div>
            <Tabs onSelect={this.handleSelect} >
              <TabList>
                <Tab>Condition List</Tab>
                <Tab>Action List</Tab>
              </TabList>
              <TabPanel className="e-action-tabPanel">
                <div className={"sortable" + this.state.ConditionList.length === 0 ? ' hide' : ''}>
                  <SortableList
                    data={this.state.ConditionList}
                    onEditClick={this.editConditionHandler}
                    onRemoveClick={this.removeConditionHandler}
                    type="condition" />
                </div>
                <div className={this.state.ConditionList.length > 0? 'hide': 'e-action-btn'}>
                  <div onClick={() => this.gotoCondtionSingle(-1, -1)} className="add-action-btn">
                    <div className="e-btn-outer">
                      <span>Single Event Condition </span>
                    </div>
                  </div>
                  <span className="e-divider">or</span>
                  <div onClick={() => this.gotoCondtionMulti(-1, -1)} className="add-action-btn">
                    <div className="e-btn-outer">
                      <span>Multiple Event Condition </span>
                    </div>
                  </div>
                </div>
                <div className="actionlist-btm">
                  <p className="sortable-msg pull-left">Drag &amp; Drop boxes to make the sequence</p>
                  <button onClick={() => this.gotoCondtionSingle(-1, -1)} type="button" className={addActionClsName}> + Add Action </button>
                </div>
              </TabPanel>
              <TabPanel>
                <div className="sortable">
                  <p className={this.state.ActionList.length > 0? 'hide': 'col-sm-12'}>You have no Action. Please add new Action</p>
                  <SortableList
                    data={this.state.ActionList} type="action"
                    onEditClick={this.editActionHandler}
                    onRemoveClick={this.removeActionHandler}
                  />

                </div>
                <div className="actionlist-btm">
                  <p className="sortable-msg pull-left">Drag &amp; Drop boxes to make the sequence</p>
                  <button onClick={this.addAction} type="button" className="btn-sm pull-right btn-red btn"> + Add Action </button>
                </div>
              </TabPanel>
            </Tabs>

            <div className="form-actions">
              <p style={{display:'inline-block'}}><small><sup>*</sup> - fields are mandatory</small></p>
              <button type="submit" className="btn btn-red pull-right" onClick={this.saveRule}>Save</button>
            </div>
          </form>
        </div>

        <Modal ref="createSingleCondition" id="createSingleCondition" title="Single Event Condition">
          <CreateSingleCondition
            data={
              !editingCondition || editingCondition.type !== 'single' ? null :
                  _.cloneDeep(editingCondition)
            }
            Callback={this.returnFromCSCModel} />
        </Modal>

        <Modal ref="createMultiCondition" id="createMultiCondition" title="Multiple Event Condition">
          <CreateMultiCondition
            data={
              !editingCondition || editingCondition.type !== 'multi' ? null :
                  _.cloneDeep(editingCondition)
            }
            Callback={this.returnFromCMCModel} />
        </Modal>

        <Modal ref="addAction" id="addAction" title="Add Actoin" actions={customActions}>
          <form className="addactionFomr">
            <div className="form-group">
              <label className="col-sm-3">Select Action</label>
              <select value={this.state.ActionType}
                className="col-sm-6"
                onChange={this.setActionType} ref="actiontype">
                <option value="">Select</option>
                <option value="SetState">SetState</option>
                <option value="ExecuteCommand">ExecuteCommand</option>
                <option value="PublishMessage">PublishMessage</option>
                <option value="Delay">Delay</option>
                <option value="TagMessage">TagMessage</option>
              </select>
            </div>
            <div className="action-form">
              {actionform}
            </div>
          </form>
        </Modal>

        <Modal ref="addCondition" id="addCondition" title="Add Condition" actions={addConditionActions}>
          <form className="addConditionFrom">
            <div className=" bordered addConditionFrom">
              <div className="form-group">
                <label className="col-sm-4">Number of Events <sup>*</sup></label>
                <div className="col-sm-5">
                  <select>
                    <option value="">Select</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </div>
              </div>
              <div className="form-group">
                <label className="col-sm-4">Enter time</label>
                <div className="col-sm-5">
                  <input type="text" placeholder="Enter time in seconds" />
                </div>
              </div>
              <div className="form-group">
                <label className="col-sm-4">Select data type <sup>*</sup></label>
                <div className="col-sm-5">
                  <select>
                    <option value="">Select</option>
                    <option value="1">String</option>
                    <option value="2">Bool</option>
                    <option value="3">Date</option>
                  </select>
                </div>
              </div>
              <div className="form-group">
                <label className="col-sm-4">Select Property <sup>*</sup></label>
                <div className="col-sm-5">
                  <input type="text" placeholder="Enter path" />
                </div>
              </div>
              <div className="form-group">
                <label className="col-sm-4">Select operand <sup>*</sup></label>
                <div className="col-sm-5">
                  <select>
                    <option value="">Select</option>
                    <option value="1">=</option>
                    <option value="2">-</option>
                    <option value="3">+</option>
                  </select>
                </div>
              </div>
              <div className="form-group">
                <label className="col-sm-4">Enter value <sup>*</sup></label>
                <div className="col-sm-5">
                  <input type="text" placeholder="Enter value" />
                </div>
              </div>
            </div>
          </form>
        </Modal>
      </div>

    );
  },


});

module.exports = CreateRule;
