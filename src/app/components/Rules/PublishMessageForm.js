var React = require('react');
var History = require('react-router').History;
var DeepLinkedStateMixin = require('react-deep-link-state');
var DataGenerator = require('../../services/DataGenerator.js');
var _ = require('lodash');

var PublishMessageForm= React.createClass({
  mixins: [History, DeepLinkedStateMixin],
  getInitialState: function() {
    return this.createFreshState();
  },
  createFreshState:function() {
    var data = DataGenerator.GenerateActionData(3);
    let argPairs = _.toPairs(data.args);
    return {
      Action:data,
      argPairs,
    };
  },
  clear:function() {
    this.setState(this.createFreshState());
  },
  componentDidMount: function() {
    let {data} = this.props;
    if (data && data.type === 'PublishMessage') {
      this.initializeFields(_.cloneDeep(data));
    }
  },
  componentWillReceiveProps:function(nextProps) {
    if(nextProps.data && this.props.data != nextProps.data && nextProps.data.type === 'PublishMessage') {
      this.initializeFields(_.cloneDeep(nextProps.data));
    }
  },
  initializeFields:function(initData) {
    this.setState({
      Action: _.cloneDeep(initData),
      argPairs: _.toPairs(initData.args),
    });
  },
  isFormValid: function() {
    console.log('PublishMessageForm.isFormValid: ', this.state.Action);
    let action = this.state.Action;
    return action.topic &&
           _.every(this.state.argPairs, p => p[0] && p[1]);
  },
  updateArgKey: function(i, e) {
    this.state.argPairs[i][0] = e.target.value;
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },

  updateArgValue: function(i, e) {
    this.state.argPairs[i][1] = e.target.value;
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },
  AddAnotherArg:function(){
    this.state.argPairs.push(['', '']);
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },
  removeArg:function(i) {
    this.state.argPairs.splice(i, 1);
    this.state.Action.args = _.fromPairs(this.state.argPairs);
    this.forceUpdate();
  },
  render: function () {
    let action = this.state.Action;

    var ActionArgList = this.state.argPairs.map(function(argPair, i){
      return(
        <div>
          <div className="form-group">
            <label className="col-sm-4">Argument</label>
            <div className="col-sm-5">
              <input type="text"
                value={argPair[0]}
                onChange={this.updateArgKey.bind(this, i)}
                placeholder="key"
                style={{width:'45%'}}
                /> :
              <input type="text"
                value={argPair[1]}
                onChange={this.updateArgValue.bind(this, i)}
                placeholder="value"
                style={{width:'45%'}}
                />
            </div>
            {
              <div className="col-sm-3 pull-right">
                <button type="button"
                  className="btn btn-red"
                  onClick={this.removeArg.bind(this, i)}>
                  Remove
                </button>
              </div>
            }
          </div>
        </div>
      );
    },this);

    return (
      <div className="PublishMessageForm bordered addactionform">
        <div className="form-group">
          <label className="col-sm-4">Topic<sup>*</sup></label>
          <div className="col-sm-5">
            <input
              type="text" placeholder="Enter topic"
              valueLink={this.deepLinkState(['Action', 'topic'])}/>
          </div>
        </div>
        <fieldset className=" primary">
          {ActionArgList}
          <div className="col-sm-4" style={{"float":"right"}}>
            <button type="button" className="btn btn-red btn-sm"  onClick={this.AddAnotherArg}>Add an argument</button>
          </div>
        </fieldset>
        {
          !this.props.showValidationErrors ||
          this.isFormValid() ? null :
            <div className="form-error-message">
              All field are mandatory
            </div>
        }
      </div>
    );
  },
});


module.exports =  PublishMessageForm
