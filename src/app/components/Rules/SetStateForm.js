var React = require('react');
var History = require('react-router').History;
var DataGenerator = require('../../services/DataGenerator.js');
var DeepLinkedStateMixin = require('react-deep-link-state');
var LinkedStateMixin = require('react-addons-linked-state-mixin');
var SmartSelect = require('../SmartSelect.js');
var ConditionPopulator = require('../../lib/ConditionPopulator.js');
var _ = require('lodash');
var {getPathsAndPropertiesOf} = require('../Conditions/common.js');


var SetStateForm= React.createClass({
  getInitialState: function() {
    this.conditionPopulator = new ConditionPopulator();
    return this.createFreshState();

  },
  mixins: [History, DeepLinkedStateMixin, LinkedStateMixin],

  componentDidMount: function() {
    console.log('SetStateForm.componentDidMount: data', this.props.data);
    let {data} = this.props;
    this.conditionPopulator.getTimeFormats(result => {
      this.setState({timeFormats: result});

      if (data && data.type === 'SetState') {
        this.initializeFields(_.cloneDeep(data));
      }
    });
  },
  componentWillReceiveProps:function(nextProps) {
    console.log('SetStateForm.componentWillReceiveProps: data', nextProps.data);
    if(nextProps.data && this.props.data != nextProps.data && nextProps.data.type === 'SetState') {
      this.initializeFields(_.cloneDeep(nextProps.data));
    }
  },
  clear:function() {
    console.log('clearing, timeFormats', this.state.timeFormats);
    this.setState({
      ...this.createFreshState(),
      timeFormats: this.state.timeFormats,
    });
  },
  createFreshState: function() {
    var data = DataGenerator.GenerateActionData(1);
    let delay = this.getTimeValueAndUnitFromString(data.Delay);
    return {
      Action:data,
      labelOrThing: '',
      paths: [],
      properties: [],
      ...delay,
      timeFormats: {},
    };
  },
  initializeFields:function(initData) {
    console.log('SetStateForm.initializeFields started');
    let resPromise = getPathsAndPropertiesOf(initData.labelName, initData.thingName);
    resPromise.then((res) => {
      this.state.Action = _.cloneDeep(initData);
      let delay = this.getTimeValueAndUnitFromString(initData.Delay);
      console.log('SetStateForm.initializeFields delay: ', delay,
                  ', timeFormats: ', this.state.timeFormats);
      this.setState({
        properties: res.properties,
        paths: res.paths,
        labelOrThing: initData.labelName ? 'Label' : 'Thing',
        ...delay,
      });
      console.log('SetStateForm.initializeFields done');
    });
  },
  getTimeValueAndUnitFromString(s) {
    let match = /(\d+)\s+(.*)/.exec(s);
    if (!match) return {delayTimeValue: 0, delayTimeUnit: '', delayChecked: !!match};
    console.log('getTimeValueAndUnitFromString: ', match[1], match[2]);
    return {delayTimeValue: match[1], delayTimeUnit: match[2], delayChecked: !!match};
  },
  isFormValid: function() {
    let action = this.state.Action;
    console.log('SetStateForm.isFormValid delay: ', action.Delay);
    return (action.labelName || action.thingName) &&
           _.every(action.states, s => s.property && s.value) &&
           (!this.state.delayChecked || action.Delay);
  },

  populateByLabelOrThing:function(labelOrThing) {
    let update = (paths) => {
      console.log('updating paths', paths);
      delete this.state.Action['labelName'];
      delete this.state.Action['thingName'];
      this.state.paths = paths;
      this.state.properties = {};
      this.forceUpdate();
    };

    if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabels(update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThings(update);
    } else {
      update([]);
    }

  },

  populateByPath:function(labelOrThing, path) {
    let update = (properties) => {
      //this.state.Action.property = '';
      this.state.properties = properties;
      this.forceUpdate();
    };

    if (!path) {
      update([]);
    } else if (labelOrThing === 'Label') {
      this.conditionPopulator.getLabelProperties(path, update);
    } else if (labelOrThing === 'Thing') {
      this.conditionPopulator.getThingProperties(path, update);
    } else {
      update([]);
    }
  },

  labelOrThingChanged:function(v) {
    this.state.labelOrThing = v;
    this.populateByLabelOrThing(v);
    this.forceUpdate();
  },

  pathChanged:function(v) {
    let action = this.state.Action;
    delete action['labelName'];
    delete action['thingName'];
    if (this.state.labelOrThing === 'Label') {
      action.labelName = v;
    } else if (this.state.labelOrThing === 'Thing') {
      action.thingName = v;
    }
    this.populateByPath(this.state.labelOrThing, v);
    this.forceUpdate();
  },

  updateStateProperty: function(i, v) {
    this.state.Action.states[i].property = v;
    this.forceUpdate();
  },

  updateStateValue: function(i, e) {
    this.state.Action.states[i].value = e.target.value;
    this.forceUpdate();
  },

  delayCheckedChanged: function(e) {
    let checked = e.target.checked;
    if (checked) {
      this.updateDelay(this.state.delayTimeValue,
                       this.state.delayTimeUnit);
    } else {
      this.updateDelay(null, null);
    }
    console.log('delayCheckedChanged', e.target.value, checked);
    this.setState({delayChecked: checked});
  },

  delayTimeValueChanged: function(e) {
    console.log('delayTimeValueChanged');
    let v = Number(e.target.value);
    this.updateDelay(v, this.state.delayTimeUnit);
    this.setState({delayTimeValue: e.target.value});
  },

  delayTimeUnitChanged: function(newUnit) {
    console.log('delayTimeUnitChanged');
    this.updateDelay(this.state.delayTimeValue, newUnit);
    this.setState({delayTimeUnit: newUnit});
  },
  updateDelay(v, u) {
    console.log('updateDelay: ', v, u);
    let n = Number(v);
    if (Number.isFinite(n) && n > 0 && u) {
      this.state.Action.Delay = v + ' ' + u;
    } else {
      this.state.Action.Delay = '';
    }
  },

  AddAnotherState:function(){
    var actionstate = this.state.Action.states;
    var newState = {property:'',value:''}
    actionstate.push(newState);
    this.forceUpdate();
  },
  removeState:function(i) {
    this.state.Action.states.splice(i,1);
    this.forceUpdate();
  },

  render: function () {

    console.log('SetStateForm.render: ', this.state);
    let action = this.state.Action;
    let isLabel = this.state.labelOrThing === 'Label';
    let isThing = this.state.labelOrThing === 'Thing';
    let pathSelected = action.labelName && isLabel || action.thingName && isThing;
    let pSensor = this.state.properties.sensor || [];
    let pState = this.state.properties.state || [];
    let properties = [];
    console.log('pSensor: ', pSensor);
    console.log('pState: ', pState);
    if (pathSelected) {
      properties.push({
        label: 'Readings',
        items: pSensor.map(p => {
          return {label: p, value: p};
        })
      });
      if (isThing) {
        properties.push({
          label: 'Devices',
          items: pState.map(p => {
            return {label: p, value: p};
          })
        });
      }
    }

    var ActionStateList = this.state.Action.states.map(function(item, i){
      return(
        <div>
          <div className="form-group" style={{"border":"none"}} key={i}>
            <label htmlFor="state_property" className="col-sm-4">Property<sup>*</sup></label>
            <div className="col-sm-5">
              <SmartSelect
                disabled={!pathSelected}
                value={item.property}
                onSetValue={this.updateStateProperty.bind(this, i)}
                groupedItems={properties} />
            </div><br/>
            <label htmlFor="state_property" className="col-sm-4">Value <sup>*</sup></label>
            <div className="col-sm-5">
              <input value={item.value} type="text"
                onChange={this.updateStateValue.bind(this, i)} />
            </div>
            {
              i === 0 ? null :
                <div className="col-sm-3 pull-right">
                  <button type="button"
                    className="btn btn-red"
                    onClick={this.removeState.bind(this, i)}>
                    Remove
                  </button>
                </div>
            }
          </div>
        </div>
      );
    },this);


    return (
      <div className="SetStateForm bordered addactionform">
        <div className="form-group">
          <div className="col-sm-4">
            <SmartSelect
              value={this.state.labelOrThing}
              firstEmpty={true}
              onSetValue={this.labelOrThingChanged}
              items={[
                  {label: 'Thing ID', value: 'Thing'},
                  {label: 'Label ID', value: 'Label'},
                ]} />
          </div>
          <div className="col-sm-5">
            <SmartSelect
              disabled={!this.state.labelOrThing}
              firstEmpty={true}
              value={isLabel ? this.state.Action.labelName : this.state.Action.thingName}
              onSetValue={this.pathChanged}
              items={
                this.state.paths.map(p => {
                  return {label: p, value: p};
                })
              } />
          </div>
        </div>

        <fieldset className=" primary">
          {ActionStateList}
          <div className="col-sm-2" style={{"float":"right"}}>
            <button type="button" className="btn btn-red btn-sm"  onClick={this.AddAnotherState}>Add</button>
          </div>
        </fieldset>
        <div className="form-group">
          <div className="checkbox col-sm-3">
            <input type="checkbox"
              id="set_state_delay"
              checked={this.state.delayChecked}
              onChange={this.delayCheckedChanged}
            />
            <label htmlFor="set_state_delay">Delay</label>
          </div>
          <div className="col-sm-3">
            <input
              disabled={!this.state.delayChecked}
              value={this.state.delayTimeValue}
              onChange={this.delayTimeValueChanged}
              type="number" />
          </div>
          <div className="col-sm-3">
            <SmartSelect
              disabled={!this.state.delayChecked}
              value={this.state.delayTimeUnit}
              onSetValue={this.delayTimeUnitChanged}
              items={
                _.map(this.state.timeFormats, (v,k) => {
                  return {label: k, value: v};
                })
              } />
          </div>
        </div>
          {
            !this.props.showValidationErrors ||
            this.isFormValid() ? null :
              <div className="form-error-message">
                All field are mandatory
              </div>
          }
      </div>
    );
  },
});

module.exports = SetStateForm;
