var React = require('react');
var History = require('react-router').History;
var DeepLinkedStateMixin = require('react-deep-link-state');
var DataGenerator = require('../../services/DataGenerator.js');
var _ = require('lodash');

var TagMessageForm= React.createClass({
  mixins: [History, DeepLinkedStateMixin],
  getInitialState: function() {
    return this.createFreshState();
  },
  createFreshState: function() {
    var data = DataGenerator.GenerateActionData(5);
    console.log(data);
    return {
      Action:data,
    };
  },
  clear:function() {
    this.setState(this.createFreshState());
  },
  componentDidMount: function() {
    let {data} = this.props;
    if (data && data.type === 'TagMessage') {
      this.initializeFields(_.cloneDeep(data));
    }
  },
  componentWillReceiveProps:function(nextProps) {
    if(nextProps.data && this.props.data != nextProps.data && nextProps.data.type === 'TagMessage') {
      this.initializeFields(_.cloneDeep(nextProps.data));
    }
  },
  initializeFields:function(initData) {
    this.setState({
      Action: _.cloneDeep(initData),
    });
  },

  isFormValid() {
    return this.state.Action.tagName;
  },

  render: function () {
  return (
    <div className="TagMessageForm bordered addactionform">
      <div className="form-group">
        <label className="col-sm-4">tagName <sup>*</sup></label>
        <div className="col-sm-5">
          <input type="text" placeholder="Enter tag name" valueLink={this.deepLinkState(['Action', 'tagName'])} />
        </div>
      </div>
      {
        !this.props.showValidationErrors ||
        this.isFormValid() ? null :
          <div className="form-error-message">
            All field are mandatory
          </div>
      }
    </div>
    );
},
});

module.exports = TagMessageForm;
