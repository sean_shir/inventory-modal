var React = require('react');
var History = require('react-router').History;

var State = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],
  
  //console.log('This is a check point')

  render: function () {
	return (
	<div>
	 {this.props.children}
	 </div>
	);
  },
});

module.exports = State;