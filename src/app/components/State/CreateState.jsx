var React = require('react');
var History = require('react-router').History;
var DataGenerator = require('../../services/DataGenerator.js'); 

var State = DataGenerator.GenerateBlankState()

var CreateState = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],
  
  getInitialState: function() {
      return {
        StateList: [],
        State:State,
        removeid:'',
        error:[]
      };
    },
  componentDidMount: function() {
  	var id = Number(this.props.params.StateId)
  	if(this.props.params.StateId != undefined){ 
  	$.get('http://localhost:3004/states', function(result) {
  			
            var collection = result.content ? result.content : result;
            if (this.isMounted()) {
              this.setState({
                State: collection[id]
              });
              State = collection[id]
            }
          }.bind(this));
  	}
  	else{
  		State = DataGenerator.GenerateBlankState();
  		this.setState({State:State})
  	}

  	
  	
  },
  updateType:function(value){
  	State['type']= value;
  	this.setState({State:State})
  },
  updateValue:function(key, event){
  	State[key] = event.target.value;
  	this.setState({State:State})
  },
  saveState:function(e){
    e.preventDefault();
  	var self = this;
  	this.setState({error:[]})
  	if(this.formValidate()){
	  	if(this.props.params.StateId != undefined){
	  		var id = this.props.params.StateId;
	  		$.ajax({
			   url: 'http://localhost:3004/states'+id,
         type: 'PUT',
         headers: {
          'Content-Type': 'application/json'
        },
			   data: State,
			   success: function(response) {
			     self.history.pushState(null, "/State/");
			   }
			});
	  	}
	  	if(this.props.params.StateId== undefined){
        var text = "";
        console.log("This is posted state:",State)
        $.ajax({
          url:'http://localhost:3004/states',
          type:"POST",
          data: State,
          headers: {
          'Content-Type': 'application/json'
        },
          success: function(){
                self.history.pushState(null, "/State/");
          }
        })



    // 	$.post('http://192.168.8.101:10022/ts/vt/v1/meta/global/states/',State, function(result) {
	  	// 	self.history.pushState(null, "/State/");
	  	// });
		}
	}
  },
  formValidate:function(){
  	var error= []
  	if(State.name == ''){
  		error.push("name can't be blank");
  		this.setState({error:error})
  		
  	}
  	if(State.type== ''){
  		error.push("please choose state type");
  		this.setState({error:error})
  		
  	}
  	if(error.length > 0){
  		return false
  	}
  	else if(error.length < 1){
  		return true
  	}

  },
  render: function () {
    var error = this.state.error.map(function(item,i){
    	return (
    		<p key={i}>{item}</p>
    		
    		)
    })

     


return (
	<div className="create-state">
		<div className="page-header">
			<h2>+ Create State Type</h2>
			<div className="page-header-link pull-right">
				<a href="#/State" className="btn btn-red btn-icon"><i className="fa fa-angle-left btn-left-icon"></i> Back</a>
			</div>
		</div>
		<div className="page-content">
		<div className={error.length > 0? 'alert alert-danger col-sm-6':'hide'}>
    			{error}
    		</div>
			<form>
				<div className="form-group">
					<label>Name</label>
					<input type="text" className="wauto" placeholder="Enter Name (lowercase)" value={this.state.State.name} onChange={this.updateValue.bind(this, 'name')} />
				</div>
				<div className="form-group">
					<p><label>Description</label></p>
					<p><textarea value={this.state.State.description} placeholder="Please enter the description (optional)"  onChange={this.updateValue.bind(this, 'description')} ></textarea></p>
				</div>
				<fieldset>
					<legend>+ State Type</legend>
					<div className="form-group">
						<div className="col-sm-4">
							<input id="free_form" type="radio" name="statetype"  onChange={this.updateType.bind(this, 'FREE_FORM')} checked={this.state.State.type =='FREE_FORM'? true : false} />
							<label htmlFor="free_form">Free Form</label>
							
						</div>
						<div className="col-sm-6 input-range">
							<input id="range" type="radio" name="statetype"   onChange={this.updateType.bind(this, 'RANGE')} checked={this.state.State.type =='RANGE'? true : false} />
							<label htmlFor="range">Range</label>
						</div>
						<div className="col-sm-4">
							<input id="command" type="radio" name="statetype"   onChange={this.updateType.bind(this, 'COMMAND')} checked={this.state.State.type =='COMMAND'? true : false}/>
							<label htmlFor="command">Command</label>
						</div>
						<div className="col-sm-6">
							<input id="enumeration" type="radio" name="statetype"   onChange={this.updateType.bind(this, 'ENUMERATION')} checked={this.state.State.type =='ENUMERATION'? true : false}/>
							<label htmlFor="enumeration">Enumeration</label>
						</div>
						<div className="col-sm-4">
							<input id="date" type="radio" name="statetype"   onChange={this.updateType.bind(this, 'DATE')} checked={this.state.State.type =='DATE'? true : false}/>
							<label htmlFor="date">Date</label>
						</div>
					</div>
				</fieldset>
				<div className="form-actions text-center">
					<button type="submit" className="btn btn-red" onClick={this.saveState}>Save</button>
				</div>
			</form>
		</div>
	</div>
 
  );
},


});

module.exports = CreateState;