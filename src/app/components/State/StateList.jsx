var React = require('react');
var History = require('react-router').History;
var Modal = require('../core/Modal.js');


var StateList = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],

  getInitialState: function() {
      return {
        StateList: [],
        removeid:''
      };
    },
  componentDidMount: function() {
  	$.get('http://localhost:3004/states', function(result) {
            var collection =result.content ? result.content : result;
            if (this.isMounted()) {
              this.setState({
                StateList: collection
              });
            }
            console.log('This are the State list:', JSON.stringify(collection[0]))
          }.bind(this));
  },
  deleteState:function(i){
  	this.setState({removeid:i})
  	this.refs.RemoveState.show()
  },
  removeState:function(){
  	//var statelist = this.state.StateList;
  	var id = Number(this.state.removeid)
    var self = this;
    console.log(this.state.removeid)
  	//statelist.splice(id, 1);
    $.ajax({
      url: 'http://localhost:3004/states/'+id,
      type: 'DELETE',
      success: function(result) {
          $.get('http://localhost:3004/states', function(result) {
            var collection = result.content ? result.content : result;
            if (self.isMounted()) {
              self.setState({
                StateList: collection
              });
            }
          }.bind(this));
      }
  });
  	//this.setState({StateList:statelist})
    this.refs.RemoveState.hide();
  },
   HideModal:function(){
    this.refs.RemoveState.hide()
  },
  render: function () {
    var List  = this.state.StateList.map(function(item, i){
    	return(
    			<li key={i} className="list-group-item">
    				<div className="list-details">
					    <h4 className="list-group-item-heading">State Name: {item.name}</h4>
					    <p className="list-group-item-text">ID: {item.stateTypeId}</p>
					    <p className="list-group-item-text">Description: {item.description}</p>
					 </div>
					 <div className="list-action">
					 	<a href={'#/EditState/'+i} className="btn btn-red"><i className="fa fa-pencil"></i></a>
					 	<button type="button" className="btn btn-red" onClick={this.deleteState.bind(this,item.id)}><i className="fa fa-trash"></i></button>
					 </div>
				  </li>
    		)
    },this)


let customActions = [
      <a key="1" className="btn-red btn" onClick={this.removeState}>Yes</a>,
      <a key="2" className="btn btn-default " onClick={this.HideModal}>No</a>
    ]

return (
<div>
 <div className="page-header">
	<h2>List of States Types</h2>
	<div className="page-header-link pull-right">
		<a href="#/CreateState" className="btn btn-red btn-icon"><i className="fa fa-star btn-left-icon"></i> Add State &nbsp; <i className="fa fa-angle-right btn-right-icon"></i></a>
	</div>
</div>
<div className="page-content">
 <ul className="list-group">
  {List}
</ul>
</div>
  <Modal ref="RemoveState" id="RemoveState" title="Add State" actions={customActions}>
      Do you want to delete this State.
    </Modal>

</div>

  );
},
});

module.exports = StateList;
