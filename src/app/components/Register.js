var React = require('react');
var History = require('react-router').History;

var Register = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],
  doRegister:function(){

  },
  

  render: function () {
	return (
		<div className="full-page-view">
			<div className="form-pp">
				<form className="full-form register">
					<div className="panel panel-default text-center">
						<div className="panel-heading">
							<h3>Admin Registration</h3>
						</div>
						<div className="panel-body">
						<div className="label-placeholder col-md-4">

						</div>
							<div className="form-group">
								<label className="col-md-4"><i className="fa fa-star"></i>accountName</label>
								<div className="col-md-8">
									<input type="text" />
									<p>name of the account</p>
								</div>
							</div>
							<div className="form-group">
								<label className="col-md-4">organization</label>
								<div className="col-md-8">
									<input type="text" placeholder="optional"/>
									<p>name of the organization</p>
								</div>
							</div>
							<div className="form-group">
								<label className="col-md-4"><i className="fa fa-star"></i>Email</label>
								<div className="col-md-8">
									<input type="text" />
								</div>
							</div>
							<div className="form-group">
								<label className="col-md-4"><i className="fa fa-star"></i>first name</label>
								<div className="col-md-8">
									<input type="text" />
								</div>
							</div>
							<div className="form-group">
								<label className="col-md-4"><i className="fa fa-star"></i>last name</label>
								<div className="col-md-8">
									<input type="text" />
								</div>
							</div>
						</div>
					</div>
					<div className="form-actions text-center">
						<button type="submit" className="btn btn-red" onClick={this.doRegister}>Register</button>
					</div>
					
				</form>
			</div>
		</div>
	);
  },


});

module.exports = Register;