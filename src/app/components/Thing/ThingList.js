var React = require('react');
var History = require('react-router').History;
var Modal = require('../core/Modal.js');
var ThingStore = require('../../stores/ThingStore');
var ThingMixin = require('../../mixins/ThingMixin');

function getThings () {
  return { ThingList: ThingStore.getThings() }
}

ThingStore.setThings();

var ThingList = React.createClass({

  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History, ThingMixin(getThings)],

  getInitialState: function() {
      return {
        // ThingList: [],
        removeid:''
      };
    },
  componentDidMount: function() {
    let self = this
    $.ajax({
      url: 'http://localhost:3004/things',
      success: function(collections) {
        let ThingList = [];
		(collections.content != null)?(
			collections.content.map((collection,i)=>{
			  console.log('didmount',collection)
			  ThingList.push(collection)
			})
		) : "";
        self.setState({ThingList: ThingList});
      }
    })
  },
  deleteThing:function(i){
  	this.setState({removeid:i})
  	this.refs.RemoveThing.show()
  },
  removeThing:function(){
  	//var statelist = this.state.ThingList;
    // var id = Number(this.state.removeid)
  	var id = this.state.removeid
    var self = this;
    console.log(this.state.removeid)
  	//statelist.splice(id, 1);
    $.ajax({
      url: 'http://localhost:3004/things/'+id,
      type: 'DELETE',
      success: function(result) {
          $.get('http://localhost:3004/things', function(result) {
            var collection = result.content ? result.content : result;
            if (self.isMounted()) {
              self.setState({
                ThingList: collection
              });
            }
          }.bind(this));
      }
  });
  	//this.setState({ThingList:thinglist})
    this.refs.RemoveThing.hide();
  },
   HideModal:function(){
    this.refs.RemoveThing.hide()
  },
  render: function () {
    var List  = this.state.ThingList.map(function(item, i){
      // console.log(item)
    	return(
    			<li key={i} className="list-group-item">
    				<div className="list-details">
					    <h4 className="list-group-item-heading">Thing Name: {item.name}</h4>
					    <p className="list-group-item-text">Description: {item.description}</p>
					 </div>
					 <div className="list-action">
					 	<a href={'#/EditThing/'+item.id} className="btn btn-red"><i className="fa fa-pencil"></i></a>
					 	<button type="button" className="btn btn-red" onClick={this.deleteThing.bind(this,item.id)}><i className="fa fa-trash"></i></button>
					 </div>
				  </li>
    		)
    },this)

     
let customActions = [
      <a key="1" className="btn-red btn" onClick={this.removeThing}>Yes</a>,
      <a key="2" className="btn btn-default " onClick={this.HideModal}>No</a>
    ]

return (
<div>
 <div className="page-header">
	<h2>List of Thing Types</h2>
	<div className="page-header-link pull-right">
		<a href="#/CreateThing" className="btn btn-red btn-icon"><i className="fa fa-star btn-left-icon"></i> Add Thing &nbsp; <i className="fa fa-angle-right btn-right-icon"></i></a>
	</div>
</div>
<div className="page-content">
 <ul className="list-group">
  {List}
</ul>
</div>
  <Modal ref="RemoveThing" id="RemoveThing" title="Add Thing" actions={customActions}>
      Do you want to delete this Thing.
    </Modal>
	
</div>

  );
},
});

module.exports = ThingList;
