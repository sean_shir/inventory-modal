'use-strict';

var React = require('react');
var Assign = require('object-assign');
var History = require('react-router').History;
var DeepLinkedStateMixin = require('react-deep-link-state');
var ThingServ = require('../../services/ThingService.js');
var DataGenerator = require('../../services/DataGenerator.js');
var Actions = require('../../actions/Actions');
var ThingStore = require('../../stores/ThingStore');
var ThingMixin = require('../../mixins/ThingMixin');
var genThing = DataGenerator.GenerateThingData();
var ReactTags = require('react-tag-input').WithContext;


function getThing () {
  return { thing: ThingStore.getThing() }
}

var CreateThing= React.createClass({
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History, DeepLinkedStateMixin, ThingMixin(getThing)],
  getInitialState: function() {
    return {
		// thing: DataGenerator.GenerateThingData(),
		error:[],
		new: true,
		states: [],
		sensors: [],
		things: [],
		tagsParent: [],
		tagsCategory: [],
		tagsOEM: [],
		tags: [ {id: 1, text: "Apples"} ]
	};
  },
  componentWillMount () {
    let thing = Assign({},this.state.thing)
      , stateTypesElements = [];
    this.state.thing.states.map( (state,i) => {
      stateTypesElements.push(this.stateBuilder(state,i))
    })
    thing.stateTypesElements = stateTypesElements;
    this.setState({thing: thing, tagsParent: genThing.parent, tagsCategory: genThing.category, tagsOEM: genThing.oemName});
    Actions.setThing(thing);
  },
  componentDidMount () {
    var id = this.props.params.ThingId;
    id && this.getThing(id);
    this.getStates();
    this.getSensors();
    this.getThings();
  },
  getThing (id) {
    var self = this;
    this.setState({new: false})
    ThingServ.GetThingDetail(id).then(function(data){
      if (self.isMounted()) {
        self.setState({thing:data})
      }
    })
  },
  getStates () {
    self = this;
    $.ajax({
      url: 'http://192.168.8.101:10022/ts/vt/v1/meta/global/states',
      success: function(collections) {
          self.setState({states: collections});
      }
    });
  },
  getSensors () {
    self = this;
    $.ajax({
      url: 'http://192.168.8.101:10021/ts/vt/v1/meta/global/sensors',
      success: function(collections) {
        self.setState({sensors: collections});
      }
    })
  },
  getThings () {
    self = this;
    $.ajax({
      url: 'http://192.168.8.101:10023/ts/vt/v1/meta/global/things',
      success: function(collections) {
        self.setState({things: collections[0].content});
      }
    })
  },
  fieldChange (i,fieldName,event) {
    let thing = Assign({},this.state.thing)
      , states = Assign([],thing.states)
      , value = event.target == undefined ? event : event.target.value;
      
    switch (fieldName) {
      case 'parent'     : thing.parentThingName         = value; break;
      case 'category'   : thing.category                = value; break;
      case 'oemName'   : thing.oemName                  = value; break;
      case 'stateTypes' : thing.states[i].name          = value;
						  if(event.target.selectedOptions[0].label != undefined)
							thing.states[i].description   = event.target.selectedOptions[0].label;
	  break;
      case 'min'        : thing.states[i].typeValues[0] = value; break;
      case 'max'        : thing.states[i].typeValues[1] = value; break;
    }
    this.setState({thing:thing});
    Actions.setThing(thing);
  },
  changeStateTypeElement (i, state, event) {
    this.fieldChange(i,'stateTypes',event)
    let thing = Assign({},this.state.thing)
      , stateTypesElements = Assign([],this.state.thing.stateTypesElements);
    stateTypesElements[i] = this.stateBuilder(state,i);
    thing.stateTypesElements = stateTypesElements;
    this.setState({thing:thing});
    Actions.setThing(thing);
  },
  handleDelete: function(i) {
	var tags = this.state.tags;
	tags.splice(i, 1);
	this.setState({tags: tags});
  },
  handleAddition: function(i, tag) {
	var tags = this.state.tags;
	tags.push({
		id: tags.length + 1,
		text: tag
	});
	this.setState({tags: tags});
	this.addTag(i, tag);
  },
  handleParentAddition: function(tag) {
	var tags = this.state.tagsParent;
	if(tags.indexOf(tag) == -1){
		tags.push(tag);
		this.setState({tagsParent: tags});
	}
	this.fieldChange(0,'parent',tag);
  },
  handleParentDelete: function(i) {
	/*var tags = this.state.tagsParent;
	tags.splice(i, 1);
	this.setState({tagsParent: tags});*/
  },
  handleCategoryAddition: function(tag) {
	var tags = this.state.tagsCategory;
	if(tags.indexOf(tag) == -1){
		tags.push(tag);
		this.setState({tagsCategory: tags});
	}
	this.fieldChange(0,'category',tag);
  },
  handleCategoryDelete: function(i) {
	/*var tags = this.state.tagsParent;
	tags.splice(i, 1);
	this.setState({tagsParent: tags});*/
  },
  handleOEMAddition: function(tag) {
	var tags = this.state.tagsOEM;
	if(tags.indexOf(tag) == -1){
		tags.push(tag);
		this.setState({tagsOEM: tags});
	}
	this.fieldChange(0,'oemName',tag);
  },
  handleOEMDelete: function(i) {
	/*var tags = this.state.tagsParent;
	tags.splice(i, 1);
	this.setState({tagsParent: tags});*/
  },
  stateBuilder (State,i) {
    var type;
    var typeValues;
	var thingStates = [];
	this.state.things.map(function(thing){
		thingStates.push(thing.states);
	});
	if(this.state.states != null){
		this.state.states.map(function(state){
		  if (State.name == state.name){
			  type = state.type;
			  if(thingStates != null){
				  thingStates.map(function(thingState){
					thingState.map(function(s){  
						if(s.name == State.name)  
						typeValues = s.typeValues;
					});
				  });	
			  }
		  }
		});
	}
    switch (type) {
      case 'RANGE' :
        return (
          <div className="brightness-state dynamic">
            <input type="number" placeholder="min" className="form-control min-brightness" onChange={this.fieldChange.bind(this,i,'min')}/>
            <span className="form-control"> to </span>
            <input type="number" placeholder="max" className="form-control max-brightness" onChange={this.fieldChange.bind(this,i,'max')}/>
          </div>
        )
      case 'DATE' :
          return (
            <span>Auto-filled</span>
          )
		default :
//					<input id={"tag-value-"+i} type="text" className="form-control tag-plate"/>
//					<span className="add-tag pointer" onClick={this.addTag.bind(this,i)}>+</span>
			return (
				<div className={State.name+"-state gen dynamic"}>
					<ReactTags
						suggestions={typeValues}
						handleDelete={this.handleDelete}
						handleAddition={this.handleAddition.bind(this, i)} />
				</div>
			)
	}
  },
  tagBuilder (val,k,i) {
    return (
      <span key={k} className={"tagClass tag tag-"+k}>{val}
        <span className="removeClass remove-tag pointer" onClick={this.removeTag.bind(this,k,i)}>x</span>
      </span>
    )
  },
  removeTag (k,i) {
    let thing = Assign({},this.state.thing);
    thing.states[i].typeValues.splice(k,1);
    this.setState({thing:thing});
    Actions.setThing(thing);
    this.componentWillMount();
  },
  addTag (i, val) {
    let thing = Assign({},this.state.thing)
      , value = val
      , k     = thing.states[i].typeValues.length
    thing.states[i].typeValues.push(value)
    this.setState({thing:thing});
    Actions.setThing(thing);
    this.componentWillMount();
	/*let thing = Assign({},this.state.thing)
      , value = $("#tag-value-"+i).val()
      , k     = thing.states[i].typeValues.length
    thing.states[i].typeValues.push(value)
    this.setState({thing:thing});
    Actions.setThing(thing);
    this.componentWillMount();*/
  },
  addDate (i) {
    // let thing = Assign({},this.state.thing)
    //   , value = $("#tag-value-"+i).val()
    //   , k     = thing.states[i].typeValues.length
    // thing.states[i].typeValues.push(value)
    // this.setState({thing:thing});
    // Actions.setThing(thing);
    // this.componentWillMount();
  },
  removeState (i) {
    let thing = Assign({},this.state.thing)
    thing.states.splice(i,1);
    thing.stateTypesElements.splice(i,1);
    this.setState({thing:thing});
    Actions.setThing(thing);
    this.componentWillMount();
  },
  addState () {
    let thing = Assign({},this.state.thing)
      , gen = DataGenerator.GenerateThingData()
      , state = gen.states[0]
      , i = this.state.thing.states.length;
    thing.states.push(state);
    thing.stateTypesElements.push(this.stateBuilder(state,i));
    this.setState({thing:thing});
    Actions.setThing(thing);
    this.componentWillMount();
  },
  saveThing: function(e){
    var self = this;
	this.prepareThingStates();
    this.prepareThingSensors();
    e.preventDefault();
    if(this.props.params.ThingId != undefined){
      ThingServ.UpdateThing(this.props.params.ThingId,this.state.thing).then(function(response){
        self.history.pushState(null, "/Thing/");
      })
    }
    else{
      if(this.state.thing.name!="")
      {
        //debugger;
		delete this.state.thing.stateTypesElements;
        ThingServ.AddThing(this.state.thing).then(function(response){
          self.history.pushState(null,"/Thing/")
        });
      }
      else
      {
        var error = this.state.error
        error.push("name can't be empty");
        this.setState({error:error})
      }
    }
  },
  
  prepareThingStates: function(){
	  var thing = this.state.thing;
	  thing.states.map(function(state,i,states){
		  states[i].thingTypeName = thing.name;
		  states[i].thingTypeId = thing.id;
		  states[i].rn = "vrn:ts:core:global:metadata:"+ thing.name +":sensor:"+ states[i].name;
	  });
	  this.setState({
		thing: thing
	  });
  },
  
  prepareThingSensors: function(){
    var thing = this.state.thing;
    thing.sensors = [];
	if(this.state.sensors != null){
		this.state.sensors.map(function(sensor){
		  if(sensor.active){
			thing.sensors.push({
				thingTypeId : thing.id,
				thingTypeName : thing.name,
				sensorTypeId : sensor.sensorTypeId,
				accountId : thing.accountId,
				name: sensor.name,
				dataType : "",
				description: sensor.description,
				updatedBy: "new_user",
				lastUpdated : "",
				rn: "vrn:ts:core:global:metadata:"+ thing.name +":sensor:"+ sensor.name
			});
		  }
		});
	}
    this.setState({
      thing: thing
    });
  },

  goBack: function(){
    this.history.pushState(null,"/Thing/")
  },

  hideCheckboxes: function(e){
    if(e.target.className.indexOf("dont-propagate-select") < 0){
      this.setState({
        sensorsCollapsed: false
      });
    }
  },

  showCheckboxes: function(){
    this.setState({
      sensorsCollapsed: true
    });
  },

  triggerCheckboxState: function(sensor, i){
    sensor.active = !sensor.active;
    var sensors = this.state.sensors;
    sensors[i] = sensor;
    this.setState({
      sensors: sensors
    });
  },

  renderSensorOption: function(sensor, i){

    return (
      <label htmlFor={"sensor-"+i} title={sensor.description} className="dont-propagate-select" key={"sensor-"+i}>
        <input className="dont-propagate-select" type="checkbox" id={"sensor-"+i} onClick={this.triggerCheckboxState.bind(this, sensor, i)}/>
        <span className="dont-propagate-select" title={sensor.description}>{sensor.name}</span>
      </label>
    )
  },

  renderSensorsDropdown: function(){
    var dropdownStyle = {
      display : this.state.sensorsCollapsed ? 'block' : 'none'
    };
    return (
      <div className="col-xs-3">
        <div className="multiselect dont-propagate-select">
          <div className="selectBox dont-propagate-select" onClick={this.showCheckboxes}>
            <select className = "form-control dont-propagate-select" onClick={this.showCheckboxes}>
              <option>Add Sensors</option>
            </select>
            <div className="overSelect dont-propagate-select"></div>
          </div>
          <div id="checkboxes" className = "dont-propagate-select" style = {dropdownStyle}>
            {
				(this.state.sensors != null) ? (
					this.state.sensors.map(this.renderSensorOption)
				) : ""
			}
          </div>
        </div>
      </div>
    )
  },

  renderStates: function(){
    var stateMinusStyle = {
      visibility: (this.state.thing.states.length > 1) ? 'visible' : 'hidden'
    };
    return(
      <div className="col-xs-9">
        <div className="line-state">
          {
            this.state.thing.states.map( (state,i,states) => {
              return (
                <div key={i} className="form-inline">
                  <div className="form-group dynamic">
                    <label>State</label>
                    <div className="state-etg-top" style={{width : '90%'}}>
                      <select className="form-control wauto" value={states[i].name} onChange={this.changeStateTypeElement.bind(this,i,state)}>
                        {
							(this.state.states != null) ? (
								this.state.states.map( (state,i) => {
									return <option key={i} value={state.name} label={state.description}>{state.name}</option>
								})
						  ) : ""
                        }
                      </select>
                      <span className="form-control"></span>
                      {this.state.thing.stateTypesElements[i]}
					  <div style={{float : 'right'}}>
						<i className="fa fa-minus-square op op-red pointer" onClick={this.removeState.bind(this,i)} style={stateMinusStyle}></i>
					  </div>
                    </div>
                    {
                      ( state.name !== 'brightness' ) &&
                      (
                        <div className="state-etg-bottom">
                          <div id={"state-tags-"+i} className="state-tags">
                            { state.typeValues.map((val,k) => { return this.tagBuilder(val,k,i) }) }
                          </div>
                        </div>
                      )
                    }
                  </div>
                </div>
              )
            })
          }
          <div className="col-xs-12 line-addstate align-right">
            <button className="btn btn-red add-state" onClick={this.addState}>Add another state</button>
          </div>
        </div>
      </div>
    )
  },

  render: function () {
    let thing = this.state.thing
      , error = this.state.error.map(function(item,i){ return ( <p key={i}>{item}</p> ) })
    return (
      <div onClick = {this.hideCheckboxes}>
        <div className="create-state create-rule">
          <div className="page-header">
            <h2>+ Create Thing Type</h2>
            <div className="page-header-link pull-right">
              <a onClick={this.goBack} className="btn btn-red btn-icon"><i className="fa fa-angle-left btn-left-icon"></i> Back</a>
            </div>
          </div>
          <div className="page-content">
            <div className={error.length > 0? 'alert alert-danger col-sm-6':'hide'}>
              {error}
            </div>
            <form>
              <div className="form-group">
                <label>Name<sup>*</sup></label>
                <input type="text" placeholder="Enter Name (lowercase)" className="wauto" valueLink={this.deepLinkState(['thing', 'name'])}/>
              </div>

              <div className="form-inline">
                <div className="form-group parentList">
                  <label>Parent</label>
				  <ReactTags
						suggestions={this.state.tagsParent}
						handleDelete={this.handleParentDelete}
						handleAddition={this.handleParentAddition} />
                </div>
                <div className="form-group categoryList">
                  <label>Category</label>
                  <ReactTags
						suggestions={this.state.tagsCategory}
						handleDelete={this.handleCategoryDelete}
						handleAddition={this.handleCategoryAddition} />
                </div>
				<div className="form-group oemList">
                  <label>OEM Name</label>
                  <ReactTags
						suggestions={this.state.tagsOEM}
						handleDelete={this.handleOEMDelete}
						handleAddition={this.handleOEMAddition} />
                </div>
              </div>

              <div className="form-group">
                <p><label onClick={this.test}>Description</label></p>
                <p><textarea  placeholder="please enter the description (optional)" valueLink={this.deepLinkState(['thing', 'description'])}></textarea></p>
              </div>

              <fieldset>
                <legend>+ State Type</legend>
                <div className="row">
                  {this.renderStates()}
                  {this.renderSensorsDropdown()}
                </div>
              </fieldset>

              <div className="form-actions">
                <p style={{display:'inline-block'}}><small><sup>*</sup> - fields are mandatory</small></p>
                <button type="submit" className="btn btn-red pull-right" onClick={this.saveThing}>Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = CreateThing;
