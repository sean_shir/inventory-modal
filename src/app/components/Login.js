var React = require('react');
var History = require('react-router').History;
var Auth = require('../services/AuthService.js'); 

var Login = React.createClass({
 
  contextTypes: {
    router: React.PropTypes.func
  },
  mixins: [History],
  getInitialState: function() {
      return {
        errormsg :'',
        error:false
      };
    },
  doLogin:function(){
  	var self = this;
  	var username = "a";
  	var password= "";
  	//this.history.pushState(null, "/State/");
  		this.setState({errormsg:'',error:false})
  		Auth.login("username", password).then(function(result){
  			var user = result.filter(function (el) {
			    return (el.userId == username || el.email == username);
			});
			if(user.length){
				self.history.pushState(null, "/State/");
				self.setState({errormsg:'',error:false})
			}
			else{
				self.setState({errormsg:'Username and Password are wrong',error:true})
			}
  		});
  		  		
  	// e.preventDefault();
    // Here, we call an external AuthService. We’ll create it in the next step
   
 
  },

  render: function () {
	return (
		<div className="full-page-view">
			<div className="form-pp">
				<form className="full-form login">
					<h3>Sign In To Continue</h3>
					<div className="image">
						<img src="images/dummyimg.png" width="125" />
					</div>
					<div className="form-group">
						<input type="text" placeholder="Email Address Or User ID" ref="username"/>
						<input type="password" placeholder="Password" ref="password" />
					</div>
					<div className={this.state.error ? '' : 'hide'}>
				        <span className="error text-danger ">{this.state.errormsg}</span>
				    </div>
					<div className="checkbox">
						<input type="checkbox" id="remember"/>
						<label htmlFor="remember">Remember me</label>
					</div>
					<div className="form-actions">
						<button type="button" className="btn btn-red" onClick={this.doLogin}>Sign In</button>
					</div>
					<div className="form-footer">
						<a href="Forgot User ID or Password">Forgot User ID or Password</a>
						<a href="#/register" className="">Register</a>
					</div>
				</form>
			</div>
		</div>
	);
  },


});

module.exports = Login;