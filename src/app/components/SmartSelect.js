var React = require('react');
var _ = require('lodash');

/*
   It renders a <select>
   If this.props.value is invalid, selects the first item
   Also whenever the selected item is not the same as this.props.value it will
   call this.props.onSetValue callback (even after the initial render)
   We need this class because the normal <select> doesn't call onChange when
   we change the value programatically

   it takes either items or groupedItems as a prop.
   groupedItems is of form:
   [
     {label: "section A",
      items: [
        {label: "label A",
         value: "value A"},
      ],
     }
   ]

   and items is of form
   [
     {label: "label A",
      value: "value A"}
   ]
*/
var SmartSelect = React.createClass({
  componentDidMount() {
    this.ensureValidValue(this.props.value);
  },

  componentDidUpdate() {
    //console.log('=== didupdate ', this.props.value, JSON.stringify(this.props.items), JSON.stringify(this.props.groupedItems));
    this.ensureValidValue(this.props.value);
  },

  changeHandler: function(e) {
    //console.log('=== changeHandler');
    this.ensureValidValue(e.target.value);
  },

  ensureValidValue: function(value) {
    //console.log('=== ensureValidValue', value);
    let {items, groupedItems, onSetValue, disabled, firstEmpty} = this.props;
    if (disabled) return;

    if (!(items && items.length>0) &&
        !(groupedItems && groupedItems.some(g => g.items.length>0))) {
      if (value !== '') {
        //console.log('empty');
        onSetValue('');
      }
      return;
    }

    let firstValue = '', valid;
    if (!firstEmpty && items && items.length > 0) {
      firstValue = items[0].value;
    } else if (!firstEmpty && groupedItems && groupedItems.length > 0 &&
               groupedItems[0].items.length > 0) {
      firstValue = groupedItems[0].items[0].value;
    }

    if (items) {
      valid = items.some(i => i.value === value);
    } else if (groupedItems) {
      valid = groupedItems.some(g => g.items.some(i => i.value === value));
    }

    if (!valid && !(firstEmpty && value === '')) {
      //console.log('=== invalid, firstValue: ', firstValue);
      onSetValue(firstValue);
    } else if (value !== this.props.value) {
      //console.log('=== different from prop');
      onSetValue(value);
    }
  },

  render: function() {
    let {value, items, groupedItems, disabled, firstEmpty,
         onChange: NOOP, ...others} = this.props;

    let opts = [];
    if (firstEmpty) {
      opts.push(<option value={''} >{disabled ? '' : 'Select'}</option>);
    }
    if (items) {
      Array.prototype.push.apply(opts, items.map(o => {
        return <option value={o.value}>{o.label}</option>;
      }));
    } else if (groupedItems) {
      Array.prototype.push.apply(opts, groupedItems.map(g => {
        return (
          <optgroup label={g.label}>
            {
              g.items.map(i => {
                return <option value={i.value}>{i.label}</option>
              })
            }
          </optgroup>
        );
      }));
    }

    return (
      <select onChange={this.changeHandler}
        value={disabled ? '' : value} {...others}>
        {
          disabled ? null : opts
        }
      </select>
    );
  },
});

module.exports = SmartSelect;
