function ConditionPopulator() {
  /*
  this.labels = [];
  this.things = [];
  this.labelProperties = {};
  this.thingProperties = {};
  this.operators = [];
  this.functions = [];
  this.timeFormats = [];
//  this.onListsPopulated = onListsPopulated;
  */

  this.getLabels = function(cb) {
    /*
    let url = 'http://192.168.8.101:10119/ts/vt/v1/engine/rs/eb2c8d80-d1cb-11e5-899a-a78c285e0f24/rules/labels';

    $.get(url, result => {
      cb(result);
    });
    */

    setTimeout(() => {
      let labels = ["street-01", "street-02", "street-03"];
      cb(labels);
    }, 10);

  };
  this.getThings = function(cb) {
    /*
    let url = 'http://192.168.8.101:10119/ts/vt/v1/engine/rs/eb2c8d80-d1cb-11e5-899a-a78c285e0f24/rules/things';
    $.get(url, result => {
      cb(result);
    });
    */

    setTimeout(() => {
      let things = [
        "truck-04",
        "truck-05",
        "truck-02",
        "truck-03",
        "street-01-002",
        "truck-08",
        "street-01-003",
        "truck-09",
        "truck-06",
        "truck-07",
        "street-01-001",
        "truck-01",
        "truck-10"
      ];
      cb(things);
    }, 10);

  };
  this.getLabelProperties = function(path, cb) {
    /*
    let url = `http://192.168.8.101:10119/ts/vt/v1/engine/rs/eb2c8d80-d1cb-11e5-899a-a78c285e0f24/rules/label/${path}/readings`;
    $.get(url, result => {
       cb(result);
    });
    */

    setTimeout(() => {
      let result = { "sensor": [ "ontime", "motion", "temperature", "present" ] };
      cb(result);
    }, 10);

  };

  this.getThingProperties = function(path, cb) {
    /*
    let url = `http://192.168.8.101:10119/ts/vt/v1/engine/rs/eb2c8d80-d1cb-11e5-899a-a78c285e0f24/rules/thing/${path}/readings`;
    $.get(url, result => {
       cb(result);
    });
    */

    setTimeout(() => {
      let result = {
        "sensor": [ "ontime", "motion", "temperature", "present" ],
        "state": [ "brightness", "color", "flashstatus", "status" ]
      };
      cb(result);
    }, 10);

  };
  this.getOperators = function(cb) {
    /*
    let url = '***URL***';
    $.get(url, result => {
       cb(result);
    });
    */

    setTimeout(() => {
      let result = [
        ">", "<", "=", "!=", ">=", "<="
      ];
      cb(result);
    }, 10);

  };
  this.getFunctions = function(cb) {
    /*
    let url = '***URL***';
    $.get(url, result => {
       cb(result);
    });
    */

    setTimeout(() => {
      let result = { "Average": "avg", "Sum": "sum" };
      cb(result);
    }, 10);
  };

  this.getTimeFormats = function(cb) {
    /*
    let url = '***URL***';
    $.get(url, result => {
       cb(result);
    });
    */

    setTimeout(() => {
      let result = { "Milli Seconds": "milliseconds", "Seconds": "seconds", "Minutes": "minutes" };
      cb(result);
    }, 10);

  };
}

module.exports = ConditionPopulator;
