var Constants = require('../constants/Constants'),
    Dispatcher = require('../dispatchers/Dispatcher')

module.exports = {
setElementDevice: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.ELEMENT_DEVICE,
      item: item
    });
  },
  setDisplay: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.DISPLAY,
      item: item
    });
  },
  setRuleDisplay: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.RULE_DISPLAY,
      item: item
    });
  },
  setRule: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.RULE,
      item: item
    });
  },
  setOEM: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.OEM,
      item: item
    });
  },
  setOEMElements: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.OEMELEMENTS,
      item: item
    })
  },
  setAccess: function(item) {
    Dispatcher.handleViewAction({
      actionType: Constants.ACCESS,
      item: item
    })
  },
  devices: function(items) {
    Dispatcher.handleServerAction({
      actionType: Constants.DEVICES,
      items: items
    })
  },
  rules: function(items) {
    Dispatcher.handleServerAction({
      actionType: Constants.RULES,
      items: items
    })
  },
  setThing: function(thing) {
    Dispatcher.handleViewAction({
      actionType: Constants.THING,
      thing: thing
    })
  }
}
