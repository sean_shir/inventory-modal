'use strict'

var CHANGE_EVENT = 'change'
  , assign = require('object-assign')
  , Constants = require('../constants/Constants')
  , EventEmitter = require('events').EventEmitter
  , Dispatcher = require('../dispatchers/Dispatcher')
  , DataGenerator = require('../services/DataGenerator.js')
  , Thing = DataGenerator.GenerateThingData()
  , Things = []
  ;

var AppStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },
  getThing () {
    return Thing;
  },
  setThing (thing) {
    Thing = thing;
  },
  getThings () {
    return Things
  },
  setThings () {
    $.ajax({
      url: 'http://192.168.8.101:10023/ts/vt/v1/meta/global/things',
      success: function(collections) {
        let things = []
        collections.map((collection,i)=>{
          //console.log('didmount',collection)
          things.push(collection)
        })
        Things = things
      }
    })
  },
  dispatcherIndex: Dispatcher.register(function(payload) {
    var action = payload.action
    switch (action.actionType) { 
      case Constants.THING: AppStore.setThing(action.thing); break;
    }
    AppStore.emitChange();
    return true;
  })
})

AppStore._maxListeners = 100;

module.exports = AppStore;
