'use strict'

var _ = require('lodash'),
    React = require('react'),
    on = '/images/bulbon.png',
    off = '/images/bulboff.png',
    Switch = require('./Switch'),
    Details = require('./Details'),
    Assign = require('object-assign'),
    Store = require('../stores/Store'),
    Mixins = require('../mixins/Mixins')
import { Table, Button } from 'react-bootstrap'

function getElement() { return { things: Store.getElementDevice().things } }

module.exports = React.createClass({
  mixins: [Mixins(getElement)],
  getInitialState: function() {
    return {
      show: false,
      i: '',
    }
  },
  thead: function() {
    return (
      <tr>
        <th column="icon"><em></em></th>
        <th column="type"><em>Type</em></th>
        <th column="stat"><em>Status</em></th>
        <th column="iden"><em>ID</em></th>
        <th column="deta"><em></em></th>
      </tr>
    )
  },
  tbody: function() {
    return (
      <tbody>
        {this.state.things.map((r,i)=>{
          let type = this.typeWriter(r.thingCatagory),
              stat = r.actions == null ? undefined : (<Switch i={i} switch={0} state={this.bulbState}/>)
          return(
            <tr key={i}>
              <td className="txt-center"><img className="bulb-cell" src={r.icon}/></td>
              <td>{type}</td>
              <td>{stat}</td>
              <td>{r.virtualId}</td>
              <td className="txt-center"><span className="btn-details" onClick={this.modal.bind(null,i)}>details</span></td>
            </tr>
          )
        })}
      </tbody>
    )
  },
  bulbState: function(i,light) {
    let things = Assign([],this.state.things)
    things[i].icon = light ? on : off
    this.setState({things:things})
  },
  typeWriter: function(t) { return t == 'actuator' ? 'Light' : _.capitalize(t) },
  modal: function(i) { this.setState({show:true,i:i}) },
  apply: function() { console.log('apply') },
  hide: function() { this.setState({show:false}) },
  render: function() {
    let classes = "table-striped table-condensed table-hover",
        apply = (<Button className="red-btn" onClick={this.hide}>Close</Button>)

    return (
      <div>
        <Table className={classes}>
          <thead>{this.thead()}</thead>
          {this.tbody()}
        </Table>
        <Details show={this.state.show} i={this.state.i} hide={this.hide} cancel={apply}/>
      </div>
    )
  }
})
