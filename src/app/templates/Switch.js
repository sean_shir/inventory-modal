'use strict'

var React = require('react')

module.exports = React.createClass({
  getInitialState: function() {
    return { switch: this.props.switch || 0 }
  },
  switch: function() {
    this.setState({switch:!this.state.switch},function(){this.props.state(this.props.i,this.state.switch)})
  },
  change: function() {},
  render: function() {
    return (
    	<label className="switch switch-flat">
  			<input type="checkbox" ref="checkbox" className="switch-input" checked={this.state.switch} onChange={this.change} onClick={this.switch}/>
  			<span className="switch-label" data-on="on" data-off="off"></span>
  			<span className="switch-handle"></span>
  		</label>
    )
  }
})
