'use strict'

var _ = require('lodash'),
    React = require('react'),
    Store = require('../stores/Store'),
    Mixins = require('../mixins/Mixins')
import { Modal, Row, Col, FormControls, Input } from 'react-bootstrap'

function getElement() { return { device: Store.getElementDevice() } }

module.exports = React.createClass({
  mixins: [Mixins(getElement)],
  render: function() {
    let p = this.props,
        e = this.state.device,
        t = e.things[p.i] || []
    return (
      <Modal show={p.show} onHide={p.hide}>
        <Modal.Body>
          <div className="element-modal">
            <Row className="modal-spec">
              <div className="row-wrapper">
                <Col xs={2}>
                  <img width="120" src={t.icon}/>
                </Col>
                <Col xs={10}>
                  <form className="form-horizontal">
                    <FormControls.Static label="Name" labelClassName="col-md-4" wrapperClassName="col-md-8" value={e.name}/>
                    <FormControls.Static label="Category" labelClassName="col-md-4" wrapperClassName="col-md-8" value={_.capitalize(t.thingCatagory)}/>
                    <FormControls.Static label="ID" labelClassName="col-md-4" wrapperClassName="col-md-8" value={t.id}/>
                  </form>
                </Col>
              </div>
            </Row>
            <Row className="modal-desc">
              <div className="row-wrapper">
                <Col md={12}>
                  <div className="desc-title">Description:</div>
                  <div className="desc-body">{t.description}</div>
                </Col>
              </div>
            </Row>
            <Row className="modal-foot">
              <div className="row-wrapper">
                <Col md={12} className="space-btn-r">
                  {p.cancel}
                </Col>
              </div>
            </Row>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
})
