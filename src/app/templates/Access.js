'use strict'

var React = require('react'),
    Store = require('../stores/Store'),
    UserMenu = require('../components/user/UserMenu'),
    AdminMenu = require('../components/user/AdminMenu')

module.exports = React.createClass({
  render: function() {
    let userMenu = Store.getAccess() ? (<UserMenu/>) : ''
    return (
      <div className="access" id="access">
        <AdminMenu userMenu={userMenu}/>
        {this.props.children}
      </div>
    )
  }
})
