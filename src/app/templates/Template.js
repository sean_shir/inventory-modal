'use strict'

var React = require('react')
  , URLS = { 0: '/devices', 1: '/rules' }
  , Menu = require('./Menu')
  , Actions = require('../actions/Actions')
import { Grid, Row } from 'react-bootstrap'

module.exports = React.createClass({
  activateLink: function(l) { 
    if(l) Actions.setRuleDisplay(0)
    this.props.history.pushState(null,URLS[l])
  },
  render: function() {
    return (
      <div className="template" id="template">
        <Menu list={this.activateLink}/>
        {this.props.children}
      </div>
    )
  }
})
