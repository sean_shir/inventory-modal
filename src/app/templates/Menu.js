'use strict'

var $ = require('jquery')
  , React = require('react')
  , Link = require('react-router').Link
import { IndexLink } from 'react-router'
import { Nav, Glyphicon } from 'react-bootstrap'

module.exports = React.createClass({
  componentDidMount: function() { $('.navbar-fixed-side-left .glyphicon-plus-sign').css('top',$('html').height() - 75 + 'px') },
  disableLink: function(e) { e.preventDefault() },
  render: function() {
    return (
      <div className="navbar-fixed-side navbar-fixed-side-left">
        <Nav>
          <li className="cursor" onClick={this.props.list.bind(null,0)}><img src="/images/devices.png"/><Link to="/admin/devices" activeClassName="active" onClick={this.disableLink}>Devices</Link></li>
          <li className="cursor" onClick={this.props.list.bind(null,1)}><img src="/images/rules.png"/><Link to="/admin/rules" activeClassName="active" onClick={this.disableLink}>Rules</Link></li>
        </Nav>
        <Glyphicon glyph="plus-sign"/>
      </div>
    )
  }
})
