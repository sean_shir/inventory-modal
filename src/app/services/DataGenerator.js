module.exports =  {
  GenerateBlankState:function(){
    return  {
      "stateTypeId":"",
      "accountId":"global",
      "name":"",
      "description":"",
      "type":"",
      "updatedBy":"newuser@company.com",
      "lastUpdated":"",
      "rn":""
    }
  },
  GenerateActionData:function(id) {
    if(id==1){
    	return {
            "type": "SetState",
            "thingName": "",
            "states": [
              {
                "property": "",
                "value": ""
              }
            ],
            "Delay": ""
          }
        }
    if(id==2){
      return {
            "type": "ExecuteCommand",
            "command": "",
            "args":{
            },
            "Delay": ""
          }
        }
    if(id==3){
      return {
            "type": "PublishMessage",
            "topic": "",
            "args":{
            }
          }
        }

    if(id==4){
      return {
            "type": "Delay",
            "Delay":{
              time:"",
              type:""
            }
          }
        }
    if(id==5){
      return {
            "type": "TagMessage",
            "tagName":""
          }
        }
  },
  GenerateConditionData:function(id){
  	if(id == 'single'){
	  	return  {
	      "thingName": "",
	      "type": "single",
	      "dataType": "",
	      "property": "",
	      "operator": "",
	      "operand": ""
	    }
    }
	},
  GenerateMultiSequenceConditionData:function(id){

      if(id == 'sequence'){
        return    {
                  "type":"multi",
                  "initialCondition":{
                      "thingName":"",
                      "dataType":"",
                      "property":"",
                      "operator":"",
                      "operand":""
                  },
                  "eventFlow":{
                      "type":"sequence",
                      "numberOfEvents":"3",
                      "timeWindow":"60 seconds",
                      "events":[
                          {
                              "conditions":[
                                  {
                                      "thingName":"",
                                      "dataType":"",
                                      "property":"",
                                      "operator":"",
                                      "checkProperty":"true",
                                      "operand":""
                                  }
                              ]
                          }
                      ]
                  }
            }
        }
        if(id == 'group'){
          return {
              "type": "multi",
              "eventFlow": {
                "type": "group",
                "numberOfEvents": "3",
                "timeWindow": "60 seconds",
                "events": [
                  {
                    "conditions": [
                      {
                        "thingName": "",
                        "dataType": "",
                        "property": "",
                        "operator": "",
                        "operand": ""
                      }
                    ]

                  }
                ]
              }
            }
        }
        if(id=='aggregation'){
          return{
              "type": "multi",
              "eventFlow": {
                "type": "aggregation",
                "numberOfEvents": "3",
                "timeWindow": "60 seconds",
                "events": [
                  {
                    "conditions": [
                      {
                        "thingName": "...",
                        "dataType": "",
                        "function": "",
                        "property": "",
                        "operator": "",
                        "operand": ""
                      }

                    ]
                  }
                ]
              }
            }
        }


  },
  GenerateRuleData:function(){
  	return {
      "ruleId": "",
      "name": "",
      "description": "",
      "enabled": true,
      "expression": "",
      "condition": [],
      "actions": [],
      "updatedBy": ""
    }
  },
  GenerateSensorData:function() {
    return {
              "sensorTypeId":"",
              "accountId":"global",
              "name":"",
              "description":"",
              "updatedBy":"newuser@company.com",
              "lastUpdated": "",
              "rn":""
            }
          },
  GenerateThingData:function() {
    return{
      "accountId": "",
      "thingTypeId": "",
      "id": "",
      "name": "",
      "parent": [
        "- none -",
        "light",
        "switch",
        "smartlight"
      ],
      "category": [
        "- none -",
        "color",
        "light"
      ],
      "description": "",
      "states": [
        {
          "name": "flashpattern",
          "typeValues": []
        }
      ],
      "stateTypesElements": [],
      "sensors": []
    }

// {
//               "thingTypeId":"",
//               "parentThingTypeId":"",
//               "accountId":"global",
//               "category":"",
//               "name":"",
//               "description":"",
//               "oemName":"",
//               "parent": [
//               "- none -",
//               "light",
//               "switch",
//               "smartlight"
//             ],
//             "category": [
//                   "- none -",
//                   "color",
//                   "light"
//                 ],
//               "states":
//                 [
//                   {
//                     "thingTypeId":"",
//                     "thingTypeName":"",
//                     "stateTypeId":"",
//                     "accountId":"",
//                     "name":"",
//                     "description":"",
//                     "type":"",
//                     "typeValues":
//                       [
//                         "",
//                         ""
//                       ],
//                     "updatedBy":"newuser",
//                     "lastUpdated":"",
//                     "rn":""
//                 }
//               ],
//               "sensors":
//                 [
//                   {
//                     "thingTypeId":"",
//                     "thingTypeName":"",
//                     "sensorTypeId":"",
//                     "accountId":"global",
//                     "name":"",
//                     "description":"",
//                     "updatedBy":"newuser",
//                     "lastUpdated":"",
//                     "rn":""
//                 }

//               ],
//               "rn": ""
//             }
        }


}
