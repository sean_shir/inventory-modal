module.exports =  {
 
  
  GetThingDetail: function(id) {
  	 return $.get('http://192.168.8.101:10023/ts/vt/v1/meta/global/things/'+id, function(result) {
      		return result.content;
      });
  },
  UpdateThing:function(id, data){
  console.log(id, data)
    return $.ajax({
		   url: 'http://192.168.8.101:10023/ts/vt/v1/meta/global/things/'+id,
		   type: 'PUT',
		   data:JSON.stringify(data),
       "headers": {
        "content-type": "application/json"
      },
		   success: function(response) {
		    return response
		   }
		});
  },
  AddThing:function(data){
    console.log(data);
    return $.ajax({
		   url: 'http://192.168.8.101:10023/ts/vt/v1/meta/global/things/',
		   type: 'POST',
		   data:JSON.stringify(data),
       "headers": {
        "content-type": "application/json"
      },
		   success: function(response) {
		    return response
		   }
		});
   },
   DeleteThing:function(id){
    return $.ajax({
      url: 'http://192.168.8.101:10023/ts/vt/v1/meta/global/things/'+id,
      type: 'DELETE',
      success: function(result) {
         return result.content;
      }
  });
   }

}

//export default new AuthService();
//module.exports = AuthService;
