module.exports =  {

  GetRuleList : function() {
     return $.get('http://localhost:3004/rules', function(result) {
          return result.content ? result.content : result;
        });
  },
  GetRuleDetail: function(id) {
     return $.get('http://localhost:3004/rules/'+id, function(result) {
          return result.content ? result.content : result;
      });
  },
  UpdateRule:function(id, data){
  console.log(id, data)
    return $.ajax({
       url: 'http://localhost:3004/rules/'+id,
       type: 'PUT',
       data:JSON.stringify(data),
       "headers": {
        "content-type": "application/json"
      },
       success: function(response) {
        return response
       }
    });
  },
  AddRule:function(data){
    console.log('RuleService.AddRule, data: ', data);
    return $.ajax({
       url: 'http://localhost:3004/rules/',
       type: 'POST',
       data:JSON.stringify(data),
       "headers": {
        "content-type": "application/json"
      },
       success: function(response) {
        return response
       }
    });
   },
   DeleteRule:function(id){
    return $.ajax({
      url: 'http://localhost:3004/rules',
      data: JSON.stringify({id}),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type: 'DELETE',
      success: function(result) {
         return result.content ? result.content : result;
      }
  });
   }

}

//export default new AuthService();
//module.exports = AuthService;
