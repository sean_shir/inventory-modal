'use strict'

var Actions = require('../actions/Actions')
	, rule = [
			{
				ruleId: "",
				ruleName: "",
				ruleCategory: "",
				ruleType: "",
				enabled: true,
				updatedBy: "prasanna.kumar.maidam@verizon.com",
				trigger: {
					virtualDeviceId: "",
					sensorId: "",
					thingType: "",
					thingCategory: "",
					thingCategoryType: "",
					property: "",
					selectedOperatorConditon: "",
					operand: ""
				},
				commands: [
					{
						virtualDeviceId: "",
						actuatorId: "",
						thingType: "",
						thingCategory: "",
						thingCategoryType: "",
						deviceCommands: [
							{
								commandName: ""
							}
						]
					}
				],
				eventCondition: {
					sensorId: "",
					thingCategory: "",
					thingCategoryType: "",
					property: "",
					operand: "",
					entityType: "",
					eventProcessType: ""
				},
				eventActions: [
					{
						commands: [
							{
								thingCategory: "",
								deviceCommands: [
									{
										commandName: "",
										arguments: {
											color: ""
										}
									}
								]
							}
						]
					}
				]
			}
		]
	,	rules = rule

module.exports = {

	get_rule: function() { return rule },

	set_rule: function(r) { if(r) rule = r },

	get_rules: function() { return rules },

	set_rules: function(R) { if(R) rules = R },

	loadRules: function () {
		$.ajax({
			url: '/data-rules',
			success: function(data) {
				Actions.rules(JSON.parse(data))
			},
			error: function(e) {
				Actions.rules([])
				console.log('error',e)
			}
		})
	}

}
