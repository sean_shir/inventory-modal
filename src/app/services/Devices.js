'use strict'

var Actions = require('../actions/Actions'),
		element = [{
			virtualDeviceId: "..",
			deviceType: "..",
			name: "..",
			oem: "..",
			things: [{
				thingCatagory: "..",
				thingCatagoryType: "..",
				thingType: "..",
				id: "..",
				virtualId: "..",
				actions: null,
				description: "..",
				name: "..",
				attributes: { }
			}],
			extendedAttributes: null
		}],
		devices = element

module.exports = {

	get_element: function() { return element },

	set_element: function(e) { if(e) element = e },

	get_devices: function() { return devices },

	set_devices: function(d) { if(d) devices = d },

	loadDevices: function () {
		$.ajax({
			url: 'http://localhost:3004/devices',
			success: function(data) {
				Actions.devices(data)
			},
			error: function(e) {
				Actions.devices([])
				console.log('error',e)
			}
		})
	}

}
