module.exports =  {
 
  
  GetSensorDetail: function(id) {
  	 return $.get('http://localhost:3004/sensors/'+id, function(result) {
      		return result.content ? result.content : result;
      });
  },
  UpdateSensor:function(id, data){
  console.log(id, data)
    return $.ajax({
		   url: 'http://localhost:3004/sensors/'+id,
		   type: 'PUT',
		   data:JSON.stringify(data),
       "headers": {
        "content-type": "application/json"
      },
		   success: function(response) {
		    return response
		   }
		});
  },
  DemoApiCall:function(){
    $.ajax({
      url:'https://api.github.com/users',
      type:'GET',
      success:function(response){
        console.log('Git',response)
      }
    }); 
  },

  AddSensor:function(data){
    this.DemoApiCall();
    console.log(data);
    return $.ajax({
		   url: 'http://localhost:3004/sensors/',
		   type: 'POST',
       crossDomain: true,
       dataType: "json",
       async: true,
		   data:JSON.stringify(data),
       "headers": {
        "content-type": "application/json"
      },
		   success: function(response) {
		    return response
		   }
		});
   },
   DeleteSensor:function(id){
    return $.ajax({
      url: 'http://localhost:3004/sensors/'+id,
      type: 'DELETE',
      success: function(result) {
         return result.content ? result.content : result;
      }
  });
   }

}

//export default new AuthService();
//module.exports = AuthService;
