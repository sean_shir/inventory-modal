var React = require('react')
  , Thing = require('../stores/ThingStore')

module.exports = function(callback) {
  return {
    getInitialState: function() {
      return callback(this)
    },
    componentWillMount: function() {
      Thing.addChangeListener(this._onChange)
    },
    componentWillUnmount: function() {
      Thing.removeChangeListener(this._onChange)
    },
    _onChange: function() {
      if (this.isMounted()) {
        this.setState(callback(this))
      }
    }
  }
}
