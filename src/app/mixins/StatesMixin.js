var React = require('react')
  , States = require('../stores/StatesStore')

module.exports = function(callback) {
  return {
    getInitialState: function() {
      return callback(this)
    },
    componentWillMount: function() {
      States.addChangeListener(this._onChange)
    },
    componentWillUnmount: function() {
      States.removeChangeListener(this._onChange)
    },
    _onChange: function() {
      if (this.isMounted()) {
        this.setState(callback(this))
      }
    }
  }
}
