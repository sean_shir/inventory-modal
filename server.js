'use strict'

var express = require('express'),
    request = require('request'),
    path = require('path'),
    app = express(),
    states = 'http://localhost:3004/states',
    sensors = 'http://localhost:3004/sensors',
    things = 'http://localhost:3004/things',        
    rules = 'http://tspace-dev-vm1.eastus.cloudapp.azure.com:8081/devices/rules/list',
    meta = 'http://tspace-dev-vm1.eastus.cloudapp.azure.com:8081/devices/rules/meta',
    base =  'http://localhost:3004'

app.use(express.static(path.join(__dirname, '/build')))
   .engine('html', require('ejs').renderFile)
   .set('view engine', 'html')
   .set('views', path.join(__dirname, '/build'));

// app.get('/*', function(req,res){
//   res.sendFile(__dirname + '/build/index.html')
// })

// Add crossDomain Rest routes
app.get('/api/states', function(req,res){
  request( states , function (error, response, body) {
    //res.header('Access-Control-Allow-Origin', '*');
    console.log(body)
    res.send(body)
  })
})

app.put('api/states', function(req,res){
  // var dataset = req.body;
  // var opt = options;
  // opt.path = '/ts/vt/v1/meta/global/states';
  // opt.method = 'PUT';
  // opt.headers['Content-Length'] = Buffer.byteLength(dataset)  

  // request( opt , function (error, response, body) {
  //   res.header('Access-Control-Allow-Origin', '*');
  //   res.send(body)
  // })

  console.log("in");
  console.log("success" + (base+req.path));
  request({ url: states, headers: req.headers, body: req.body }, function(err, remoteResponse, remoteBody) {
        if (err) { return res.status(500).end('Error'); }
        console.log("success");
        //res.header('Access-Control-Allow-Origin', '*');
        res.end(remoteBody);
    });
})


app.post('/states', function(req,res){
  // var dataset = req.body;
  // console.log(dataset);
  // var opt = options;
  // opt.uri = 
  // opt.method = 'POST';
  // opt.headers['Content-Length'] = Buffer.byteLength(dataset)  

  // request( opt , function (error, response, body) {
  //   console.log(error)
  //       console.log(response)
  //           console.log(body)
  //   res.header('Access-Control-Allow-Origin', '*');
  //   res.send(body)
  // })
  console.log("in");
  console.log(req.body);
  request({ url: states, headers: req.headers, body: req.body }, function(err, remoteResponse, remoteBody) {
        if (err) { return res.status(500).end('Error'); }
        console.log("success");
        res.header('Access-Control-Allow-Origin', '*');
        res.end(remoteBody);
    });
})

app.get('/sensors', function(req,res){
  request( sensors , function (error, response, body) {
    res.header('Access-Control-Allow-Origin', '*');
    res.send(body)
  })
})
app.get('/things', function(req,res){
  request( things , function (error, response, body) {
    res.header('Access-Control-Allow-Origin', '*');
    res.send(body)
  })
})
app.get('/rules', function(req,res){
  request( rules , function (error, response, body) {
    res.header('Access-Control-Allow-Origin', '*');
    res.send(body)
  })
})



// app.get('/data-rules', function(req,res)
//   request( rules , function (error, response, body) {
//     res.send(body)
//   })
// }) 

// app.get('/*', function(req,res){
//   res.sendFile(__dirname + '/src/www/index.html')
// })

// Route not found -- Set 404
// app.get('*', function(req, res) {
//   res.send('route : Sorry this page does not accessible!')
// })

app.listen(3000, function(){
  console.log('Server is Up and Running at Port : 3000')
})
